﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LE4Configurator {
    public partial class About : tabRDWRControl {
        public About() {
            InitializeComponent();
            dataGridView1.ColumnCount = 2;
            dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

        }


        public class CntInfo {
            public string desc;
            public int n;
        }


        enum ERRS { ERR_MAGNET, ERR_OPEN_COVER, ERR_OPEN_TERMINAL, ERR_STORAGE, ERR_STORAGE_NOT_RECOVER, ERR_I2C, ERR_BATTERY,  ERR_ADC, ERR_NONE};
        List<Tuple<ERRS, string>> errDEsc = new List<Tuple<ERRS, string>>() {
             new Tuple <ERRS, string> (ERRS.ERR_MAGNET, "Магнит"),
             new Tuple <ERRS, string> (ERRS.ERR_OPEN_COVER, "Крышка счетчика"),
             new Tuple <ERRS, string> (ERRS.ERR_OPEN_TERMINAL, "Крышка клемм. колодки"),
             new Tuple <ERRS, string> (ERRS.ERR_STORAGE, "Сбой памяти"),
             new Tuple <ERRS, string> (ERRS.ERR_STORAGE_NOT_RECOVER, "Отказ памяти"),
             new Tuple <ERRS, string> (ERRS.ERR_I2C, "Шина обмена"),
             new Tuple <ERRS, string> (ERRS.ERR_BATTERY, "Батарея"),
             new Tuple <ERRS, string> (ERRS.ERR_ADC, "АЦП"),
        };
        private string parseErrs(Int16 err) {
            string str = "";
            foreach (var erd in errDEsc) {
                if ((err & (1 << (int)erd.Item1)) != 0) {
                    str += erd.Item2;
                    str += ", ";
                }
            }
            
            return str;
        }

        private string parseType(byte n) {
            string stype = n.ToString();
            try {
                StreamReader f = File.OpenText("config.txt");
                while (true) {
                    string r = f.ReadLine();
                    if (r == null)
                        break;
                    string [] sps  = r.Split(':');
                    if (sps.Count() == 2) {
                        Int32 ns = 0;
                        bool res = Int32.TryParse(sps[0], out ns);
                        if (res && (ns == n)) {
                            stype = sps[1];
                            break;
                        }
                    }

                }
                
            }
            catch {

            }
            return stype;
        }


        public override void OnRead(Protocol proto, Protocol.Commands cmdRead) {
            byte[] reply = proto.sendPacketGetReply(Protocol.Query.ENQ, (byte)Protocol.Commands.RD_COUNTER_INFO, Protocol.MAX_REPLYLEN);
            if (reply != null) {
                dataGridView1.Rows.Clear();
                dataGridView1.Rows.Add(7);

                dataGridView1.Rows[0].Cells[0].Value = "Дата производства";
                UInt32 sec1970 = BitConverter.ToUInt32(reply, 0);
                DateTime dt = new System.DateTime(1970, 1, 1).AddSeconds(sec1970);
                string s = dt.ToString("dd/MM/yyyy");                
                dataGridView1.Rows[0].Cells[1].Value = s;


                dataGridView1.Rows[1].Cells[0].Value = "Серийный";
                dataGridView1.Rows[1].Cells[1].Value = BitConverter.ToInt32(reply, 4).ToString();

                dataGridView1.Rows[2].Cells[0].Value = "Сетевой";
                dataGridView1.Rows[2].Cells[1].Value = BitConverter.ToInt32(reply, 8).ToString();

                dataGridView1.Rows[3].Cells[0].Value = "Тип счетчика";
                //dataGridView1.Rows[3].Cells[1].Value = reply[12].ToString();
                dataGridView1.Rows[3].Cells[1].Value = parseType(reply[12]);


                //skip ver HW at 13

                dataGridView1.Rows[4].Cells[0].Value = "Версия HW";
                dataGridView1.Rows[4].Cells[1].Value = reply[13].ToString();

                dataGridView1.Rows[5].Cells[0].Value = "Версия ПО";            
                dataGridView1.Rows[5].Cells[1].Value = reply[14].ToString();
                
                dataGridView1.Rows[6].Cells[0].Value = "Ошибки";
                if (reply.Length >= 17) {
                    Int16 cntErr = BitConverter.ToInt16(reply, 15);
                    dataGridView1.Rows[6].Cells[1].Value = "Код " + cntErr.ToString() + ": " + parseErrs(cntErr);
                }               



            }
            else {
                MessageBox.Show("Нет ответа");
            }
        }

    }
}