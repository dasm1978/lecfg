﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {



    public partial class eventList : tabRDWRControl {
        private void readDTChangeEvents(Protocol proto, Protocol.Commands cmdRead) {
            dataGridView1.Rows.Clear();
            List<Tuple<string, string>> listEv = new List<Tuple<string, string>>();
            int totalEv = 0;
            for (int pack = 0; pack < 30; pack++) {
                byte[] arg = new byte[1];
                arg[0] = (byte)pack;
                byte[] reply = proto.sendPacketGetReply(Protocol.Query.ENQ, (byte)cmdRead, Protocol.MAX_REPLYLEN, arg, 3000);
                if (reply == null)
                    break;
                if (reply.Length == 0)
                    break;
                int repLenB = reply.Length;
                if ((repLenB % 12) != 0)
                    break;
                int evCnt = repLenB / 12;
                totalEv += evCnt;
                for (int i = 0; i < evCnt; i++) {
                    byte[] tempArray = new byte[6];
                    Array.Copy(reply, i * 12, tempArray, 0, 6);
                    tempArray = BCD.bcd2bin(tempArray);
                    string strEvTime = String.Format("В {2:00}:{1:00}:{0:00} {3:00}-{4:00}-{5:00}",
                        tempArray[0], tempArray[1], tempArray[2], tempArray[3], tempArray[4], tempArray[5]);

                    Array.Copy(reply, i * 12 + 6, tempArray, 0, 6);
                    tempArray = BCD.bcd2bin(tempArray);
                    string strEvTo = String.Format("На {2:00}:{1:00}:{0:00} {3:00}-{4:00}-{5:00}",
                        tempArray[0], tempArray[1], tempArray[2], tempArray[3], tempArray[4], tempArray[5]);
                    listEv.Add(new Tuple<string, string>(strEvTime, strEvTo));
                }
            }
            if (listEv.Count == 0)
                return;
            dataGridView1.Rows.Clear();
            dataGridView1.ColumnCount = 3;
            dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            for (int i = 0; i < listEv.Count; i++) {
                dataGridView1.Rows.Add();
                string num = String.Format("{0:00}", i);
                dataGridView1.Rows[i].Cells[0].Value = num;
                dataGridView1.Rows[i].Cells[1].Value = listEv[i].Item1;
                dataGridView1.Rows[i].Cells[2].Value = listEv[i].Item2;
                if (i % 2 == 0)
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.LightPink;
                else
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.LightCyan;

            }
        }
        private void readOnOffEvents(Protocol proto, Protocol.Commands cmdRead, string ondesc, string offdesc) {

            dataGridView1.Rows.Clear();
            dataGridView1.ColumnCount = 3;

            dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            stringEv[] s = new stringEv[200];
            for (int i = 0; i < 200; i++)
                s[i] = new stringEv();
            int totalEv = 0;
            for (int pack = 0; pack < 30; pack++) {
                byte[] arg = new byte[1];
                arg[0] = (byte)pack;
                byte[] reply = proto.sendPacketGetReply(Protocol.Query.ENQ, (byte)cmdRead, Protocol.MAX_REPLYLEN, arg, 3000);
                if (reply == null)
                    break;
                if (reply.Length == 0)
                    break;
                int repLenB = reply.Length;
                if ((repLenB % 7) != 0)
                    break;
                int evCnt = repLenB / 7;
                totalEv += evCnt;
                for (int i = 0; i < evCnt; i++) {
                    byte[] tempArray = new byte[6 + 6 + 1];
                    Array.Copy(reply, i * 7, tempArray, 0, 7);
                    tempArray = BCD.bcd2bin(tempArray);
                    string strEvOn = String.Format("В {2:00}:{1:00}:{0:00} {3:00}-{4:00}-{5:00}",
                        tempArray[0], tempArray[1], tempArray[2], tempArray[3], tempArray[4], tempArray[5]);
                    s[pack * 16 + i].dt = strEvOn;
                    s[pack * 16 + i].ev = (tempArray[6] == 0) ? offdesc : ondesc; 
                }
            }
            Array.Resize(ref s, totalEv);
            if (s == null || s.Count() == 0) {
                return;
            }


            for (int i = 0; i < s.Length; i++) {
                dataGridView1.Rows.Add();
                string num = String.Format("{0:00}", i);
                dataGridView1.Rows[i].Cells[0].Value = num;
                dataGridView1.Rows[i].Cells[1].Value = s[i].dt;
                dataGridView1.Rows[i].Cells[2].Value = s[i].ev;
                if (s[i].ev == ondesc)
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.LightPink;
                else
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.LightCyan;

            }

        }


        public override void OnWrite (Protocol proto, Protocol.Commands cmd) {

        }

        public override void OnRead(Protocol proto, Protocol.Commands cmdRead) {            
            stringEv[] evP = new stringEv[0];
            if (cmdRead == Protocol.Commands.RD_DT_CHANGES) {
                readDTChangeEvents(proto, cmdRead);
            }
            else if (cmdRead == Protocol.Commands.RD_COVER_EV || cmdRead == Protocol.Commands.RD_TERMINAL_EV) {
                readOnOffEvents(proto, cmdRead, "Надета", "Снята");                
            }
            else if (cmdRead == Protocol.Commands.RD_MAGNET_EV) {
                readOnOffEvents(proto, cmdRead, "Начало воздействия", "Окончание воздействия");
            }

            else if (cmdRead == Protocol.Commands.RD_CURR_EV_CH_EV) {
                readOnOffEvents(proto, cmdRead, "Положительная мощность", "Отрицательная мощность");
            }

            else if (cmdRead == Protocol.Commands.RD_POWER_LIM_EXC_EV) {
                readOnOffEvents(proto, cmdRead, "Реле включено", "Реле выключено");
            }

            else
                readOnOffEvents(proto, cmdRead, "Вкл.", "Выкл.");

        }
        public eventList() {
            InitializeComponent();
        }
    }

    public class tabRDWRControl : UserControl {
        public tabRDWRControl() {

        }

        public virtual bool OnSaveToFile() {
            return false;
        }
        

        public virtual void OnRead(Protocol proto, Protocol.Commands cmdRead) {

        }

        public virtual void OnWrite(Protocol proto, Protocol.Commands cmd) {

        }

        public virtual void onAdd() {

        }

        public virtual void onDel() {

        }


    }


}
