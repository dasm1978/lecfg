﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    
    public partial class SettingsPanel : tabRDWRControl {
        public SettingsPanel() {
            InitializeComponent();
            chkTimeCurrent.Checked = true;
            dateTimePicker1.Enabled = false;
            dateTimePickerControl.Enabled = false;
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "HH:mm:ss dd-MM-yy ";
            dateTimePickerControl.Format = DateTimePickerFormat.Custom;
            dateTimePickerControl.CustomFormat = "HH:mm:ss dd-MM-yy ";
            dateTimePickerControl.ShowCheckBox = dateTimePickerControl.ShowUpDown = false;
            timer1.Start();
            for (int i = -12; i < 12; i++)
                comboBox1.Items.Add(i.ToString());
            comboBox1.SelectedItem = "0";
        }

        private Protocol m_protocol;
        public void setProto (Protocol proto) {
            m_protocol = proto;
        }
        


        public override void OnRead(Protocol proto, Protocol.Commands cmdRead) {
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.ENQ, (byte)Protocol.Commands.WR_DTIME, Protocol.MAX_REPLYLEN);
            if (reply == null)
                return;
            if (reply.Length < 4)
                return;
            DateTime dt = new DateTime();
            string dtstr = "";
            dtstr += (BCD.bcd2bin(reply[5]) + 2000).ToString() + @"-";
            dtstr += (BCD.bcd2bin(reply[4])).ToString() + @"-";
            dtstr += (BCD.bcd2bin(reply[3])).ToString() + @" ";

            dtstr += (BCD.bcd2bin(reply[2])).ToString() + @":";
            dtstr += (BCD.bcd2bin(reply[1])).ToString() + @":";
            dtstr += (BCD.bcd2bin(reply[0])).ToString();

            bool res = DateTime.TryParse(dtstr, out dt);
            if (res) {
                dateTimePickerControl.Value = dt;
            }
        }

        public override void OnWrite(Protocol proto, Protocol.Commands cmd) {
            DateTime dt = dateTimePicker1.Value;

            int offs = 0;
            bool res = Int32.TryParse(comboBox1.SelectedItem.ToString(), out offs);
            TimeSpan tsoffs = new TimeSpan(offs, 0, 0);
            dt += tsoffs; 
            byte[] packetDT = new byte[128];
            int idx = 0;
            packetDT[idx++] = BCD.bin2bcd(dt.Second);
            packetDT[idx++] = BCD.bin2bcd(dt.Minute);
            packetDT[idx++] = BCD.bin2bcd(dt.Hour);
            packetDT[idx++] = BCD.bin2bcd(dt.Day);
            packetDT[idx++] = BCD.bin2bcd(dt.Month);
            packetDT[idx++] = BCD.bin2bcd(dt.Year - 2000);
            packetDT[idx++] = BCD.bin2bcd((byte)dt.DayOfWeek);
            packetDT[idx++] = BCD.bin2bcd(0); // перевод летнее зимнее
            Array.Resize(ref packetDT, idx);
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.WR_DTIME, Protocol.MAX_REPLYLEN, packetDT);
            if (reply != null)
                MessageBox.Show("Записано");
            else
                MessageBox.Show("Не записано");
        }


        
        private void chkTimeCurrent_CheckedChanged(object sender, EventArgs e) {
            if (chkTimeCurrent.Checked) {
                dateTimePicker1.Enabled = false;
                dateTimePicker1.Value = DateTime.Now;
            }
            else {
                dateTimePicker1.Enabled = true;
            }

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e) {
            if (chkTimeCurrent.Checked) {
                dateTimePicker1.Value = DateTime.Now;
                dateTimePicker1.Enabled = false;
            }
            else {
                dateTimePicker1.Enabled = true;
            }
        }

                
        private void timer1_Tick(object sender, EventArgs e) {
            if (chkTimeCurrent.Checked) {
                dateTimePicker1.Value = DateTime.Now;
                dateTimePicker1.Invalidate();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) {

        }
    }
}

