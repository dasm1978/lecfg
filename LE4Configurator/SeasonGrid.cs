﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {

    public partial class SeasonGrid : UserControl {
        public enum eDayTypes { WEEKDAY, SATURDAY, SUNDAY, HOLIDAY };
        private int[] graphics = new int[4] {0 , 0, 0, 0} ;
        private string[] daysDesc = { "Будние", "Суббота", "Воскресенье", "Праздничные" };
        private int m_trackCell = -1;  // отслеживаем где выпадет меню контесктное
        private int maxDaysTypes = 4;// 
        
        public Date m_start;

        public string m_Name { get; set; }
        private CalendarPanel m_parent;
        public SeasonGrid(string name, CalendarPanel parent, Control showOnPanel, CSeason season) {
            InitializeComponent();
            m_parent = parent;
            this.Parent = showOnPanel;
            m_Name = name;
            m_start = season.start;
            graphics = season.index;
            this.Height = 35;
            Width = parent.Width - 5; // TODO ????
            grid.RowCount = 1;
            grid.Rows[0].Height = this.Height;

            grid.AllowUserToAddRows = false;
            grid.AllowUserToResizeColumns = false;
            grid.AllowUserToOrderColumns = false;

            grid.AllowUserToResizeRows = false;
            grid.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            grid.ColumnHeadersVisible = false;
            DrawGrid();
        }

        public void setGrapIdx (int [] i) {
            graphics = i;
        }
        public int [] getGraphsIdxs () {
            return  graphics;
        }
        public Date getDate() {
            return m_start;
        }
        public void DrawGrid() {
            grid.RowHeadersWidth = 120;
            grid.Rows[0].HeaderCell.Value = m_Name;
            grid.RowTemplate.Height = 3;
            grid.RowTemplate.MinimumHeight = 3;
            grid.ColumnCount = maxDaysTypes;
            DataGridViewCellStyle st = new DataGridViewCellStyle();
            //st.BackColor = CTariffIval.getColor(m_tariffs[i].m_tar);
            st.WrapMode = DataGridViewTriState.True;
            st.Alignment = DataGridViewContentAlignment.MiddleCenter;        
            for (int i = 0; i < maxDaysTypes; i++) {
                //Color c = Color.FromArgb(20 * graphics[i], 28 + 20 * graphics[i], 0x80);
                //st.BackColor = c;
                grid.Rows[0].Cells[i].Style = st;
                grid.Rows[0].Cells[i].Value = daysDesc[i] + Environment.NewLine + "График № "+ graphics[i].ToString();
            }
            Utilities.MakeEq(grid);
        }

        private void toolEdit_Click(object sender, EventArgs e) {
            int graphs = 1;
            if (m_parent != null)
                graphs = m_parent.getAvailableGraphs();
            GraphChoiseDlg d = new GraphChoiseDlg(graphs);
            d.setSelected(graphics[m_trackCell]);
            d.StartPosition = FormStartPosition.Manual;
            Point m_clickPoint = contextMenuStrip1.PointToScreen(new Point(0, 0));
            d.Location = PointToScreen(PointToClient(Cursor.Position));
            DialogResult res = d.ShowDialog(this);
            if (res != DialogResult.OK)
                return;
            graphics[m_trackCell] =  d.getGraph();
            DrawGrid();
            //Form1
        }

        public bool isUsed (int n) {
            for (int i = 0; i < graphics.Count(); i++) {
                if (graphics[i] == n)
                    return true;
            }
            return false;
        }

        private void grid_CellContentClick(object sender, DataGridViewCellEventArgs e) {

        }

        private void grid_CellEnter(object sender, DataGridViewCellEventArgs e) {

        }

        private void grid_CellMouseEnter(object sender, DataGridViewCellEventArgs e) {
            m_trackCell = e.ColumnIndex;
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e) {
            int iTar = m_trackCell;
            if (iTar < 0) {
                e.Cancel = true;
            }
        }
    }
    public class CSeason {
        public CSeason (Date st, int[] i) {
            start = st;
            index = i;
        }

        public CSeason() {
        }

        public Date start = new Date(1, 1);
        public int[] index = new int[4];

        public static bool operator == (CSeason dt, CSeason d) {
            if (dt.start == d.start)
                return true;
            return false;
        }

        public static bool operator !=(CSeason dt, CSeason d) {
            if (dt.start != d.start)
                return true;
            return false;
        }

        public string tostringDate() {
            return start.asString();
        }
        public string toXmlString() {
            return String.Format("{0}.{1}", start.m_day, start.m_month);
        }


        public static List <Date> makeDatesFromByteArray (byte [] ar) {
            List<Date> l = new List<Date>();
            for (int i = 0; i < ar.Count(); i += 2) {
                byte[] ab = new byte[2];
                Array.Copy(ar, i, ab, 0, 2); // берем два байта day and mon
                byte day = BCD.bcd2bin((byte)ab[0]);
                byte mon = BCD.bcd2bin((byte)ab[1] + 1);
                l.Add(new Date(day, mon));
            }
            return l;
        }

        public static CSeason constructFromStr(string startDate, string D0, string D1, string D2, string D3) {
            CSeason s = new CSeason();
            string[] st = startDate.Split('.');
            Int32.TryParse(st[0], out s.start.m_day);
            Int32.TryParse(st[1], out s.start.m_month);
            Int32.TryParse(D0, out s.index[0]);
            Int32.TryParse(D1, out s.index[1]);
            Int32.TryParse(D2, out s.index[2]);
            Int32.TryParse(D3, out s.index[3]);
            return s;
        }
    }

}
