﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class ServicePanel : UserControl {
        public ServicePanel() {
            InitializeComponent();
            boxRealU.Text = "220";
            boxRealI.Text = "5";
            boxRealP.Text = "1100";
            boxRealPQ.Text = "0";
        }

        public void setParams(Protocol proto) {
            m_protocol = proto;
        }
        private void btnWrProfIval_Click(object sender, EventArgs e) {

            int recT = (int)upDwnRecIval.Value * 60;
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.WR_PROFILE_TIME, Protocol.MAX_REPLYLEN, BitConverter.GetBytes(recT));
            if (reply != null)
                MessageBox.Show("Записано");
            else
                MessageBox.Show("Не записано");
        }

        private Protocol m_protocol;

        private void btnClrJournals_Click(object sender, EventArgs e) {
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.EX_CLR_JRNLS, Protocol.MAX_REPLYLEN);
            if (reply != null) {
                MessageBox.Show("Очищены");
            }
            else
                MessageBox.Show("Не очищены");
        }

        private void btnPPSOn_Click(object sender, EventArgs e) {
            byte[] pdata = new byte[1] { 1 };
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.EXEC_PPS_ON, Protocol.MAX_REPLYLEN, pdata);
            if (reply != null) {
                MessageBox.Show("Ok");
            }
            else
                MessageBox.Show("Нет ответа");
        }

        private void btnPPSOff_Click(object sender, EventArgs e) {
            byte[] pdata = new byte[1] { 0 };
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.EXEC_PPS_ON, Protocol.MAX_REPLYLEN, pdata);
            if (reply != null) {
                MessageBox.Show("Ok");
            }
            else
                MessageBox.Show("Нет ответа");
        }

        private void btnReadKClock_Click(object sender, EventArgs e) {
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.ENQ, (byte)Protocol.Commands.RD_RTC_COEFF, Protocol.MAX_REPLYLEN);
            if (reply == null) {
                MessageBox.Show("Нет ответа");
                return;
            }
            Int32 kc = BitConverter.ToInt32(reply, 0);
            numericUpDown1.Value = (decimal)kc;

        }

        private void btnWRKClock_Click(object sender, EventArgs e) {
            Int32 d = (Int32)numericUpDown1.Value;
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.WR_RTC_COEFF, Protocol.MAX_REPLYLEN, BitConverter.GetBytes(d));
            if (reply == null) {
                MessageBox.Show("Нет ответа");
                return;
            }
            else {
                MessageBox.Show("Ok");
            }

        }

        private void btnReadCal_Click(object sender, EventArgs e) {
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.ENQ, (byte)Protocol.Commands.RD_CALIBRATION, Protocol.MAX_REPLYLEN);
            if (reply == null)
                return;
            Single kU = BitConverter.ToSingle(reply, 0);
            boxkU.Text = kU.ToString();
            Single kI = BitConverter.ToSingle(reply, 4);
            boxkI.Text = kI.ToString();
            Single kP = BitConverter.ToSingle(reply, 8);
            boxkP.Text = kP.ToString();
            Single kQ = BitConverter.ToSingle(reply, 12);
            boxkQ.Text = kQ.ToString();

        }

        private void btnWrCal_Click(object sender, EventArgs e) {
            Single kU = 0;
            Single kI = 0;
            Single kP = 0;
            Single kQ = 0;
            try {
                if (!Single.TryParse(boxkU.Text, out kU))
                    throw new Exception("Invalid value");
                if (!Single.TryParse(boxkI.Text, out kI))
                    throw new Exception("Invalid value");
                if (!Single.TryParse(boxkP.Text, out kP))
                    throw new Exception("Invalid value");
                if (!Single.TryParse(boxkQ.Text, out kQ))
                    throw new Exception("Invalid value");                

            }

            catch (Exception ex) {
                MessageBox.Show(ex.Message);
                return;
            }
            List<byte> packet = new List<byte>();
            packet.AddRange(BitConverter.GetBytes(kU));
            packet.AddRange(BitConverter.GetBytes(kI));
            packet.AddRange(BitConverter.GetBytes(kP));
            packet.AddRange(BitConverter.GetBytes(kQ));
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.WR_CALIBRATION, Protocol.MAX_REPLYLEN, packet.ToArray());
            if (reply == null) {
                MessageBox.Show("Нет ответа");
                return;
            }
            else {
                MessageBox.Show("Ok");
            }
        }

        private void btnClearAccumulatedEnergy_Click(object sender, EventArgs e) {
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.EX_CLR_ENERGY, Protocol.MAX_REPLYLEN);
            if (reply == null) {
                MessageBox.Show("Нет ответа");
                return;
            }
            else {
                MessageBox.Show("Ok");
            }
        }

        private void btnClrPowerProfile_Click(object sender, EventArgs e) {
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.EX_CLR_POW_PROFILES, Protocol.MAX_REPLYLEN);
            if (reply == null) {
                MessageBox.Show("Нет ответа");
                return;
            }
            else {
                MessageBox.Show("Ok");
            }
        }

        private void btnClrErr_Click(object sender, EventArgs e) {
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.EX_CLR_ERRS, Protocol.MAX_REPLYLEN);
            if (reply == null) {
                MessageBox.Show("Нет ответа");
                return;
            }
            else {
                MessageBox.Show("Ok");
            }
        }

        private void btnWrK_Click(object sender, EventArgs e) {
            
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.WR_R, Protocol.MAX_REPLYLEN, BitConverter.GetBytes((UInt32)numK.Value));
            if (reply == null) {
                MessageBox.Show("Нет ответа");
                return;
            }
            else {
                MessageBox.Show("Ok");
            }

        }

        private void btnCalibr_Click(object sender, EventArgs e) {
            V92Measurements m_meas = new V92Measurements();
            byte[] rawMeas = m_protocol.sendPacketGetReply(Protocol.Query.ENQ, 0xc8, m_meas.rawLen);            
            if ((rawMeas != null) && (rawMeas.Length == m_meas.rawLen)) {
                m_meas.asRaw = rawMeas;
                V92Measurements s = m_meas;
                

                double realU = 0, realI = 0, realP = 0, realPQ = 0;
                Double.TryParse(boxRealU.Text, out realU);
                Double.TryParse(boxRealI.Text, out realI);
                Double.TryParse(boxRealP.Text, out realP);
                Double.TryParse(boxRealPQ.Text, out realPQ);

                Single kU = 0;
                Single kI = 0;
                Single kP = 0;
                Single kQ = 0;                              
                
                if (s.UAVG != 0)                    
                    kU = (float)realU / s.UAVG;
                if (s.IAAVG != 0)
                    kI = (float)realI / s.IAAVG;
                if (s.PAAVG != 0)
                    kP = (float)realP / s.PAAVG;
                if (s.QAVG != 0)
                    kQ = (float)realPQ / s.QAVG;

                List<byte> packetCalData = new List<byte>();
                packetCalData.AddRange(BitConverter.GetBytes(kU));
                packetCalData.AddRange(BitConverter.GetBytes(kI));
                packetCalData.AddRange(BitConverter.GetBytes(kP));
                packetCalData.AddRange(BitConverter.GetBytes(kQ));
                byte[] rep = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.WR_CALIBRATION, Protocol.MAX_REPLYLEN, packetCalData.ToArray());
                if (rep != null) {
                    boxkU.Text = kU.ToString();
                    boxkI.Text = kI.ToString();
                    boxkP.Text = kP.ToString();
                    boxkQ.Text = kQ.ToString();
                }


            }

        }

        class V92Measurements {
            public int FREQINST;
            public int PAINST;
            public int QINST;
            public int IAINST;
            public int UINST;
            public int PAAVG;
            public int QAVG;
            public int FREQAVG;
            public int IAAVG;
            public int UAVG; //UAVG d4
            public int x1 = 0; //d5
            public int x2 = 0; //d6
            public int x3 = 0; //d7
            public int x4 = 0; //d8
            public int UDCINST;
            public int IADCINST;
            public int x5 = 0; //db
            public int ZXDATREG;
            public int ZXDAT;
            public int PHDAT;
            public int x6 = 0; //df
            public int T8BAUD;


            public int rawLen { get { return 22 * 4; } }

            enum regsOffsets {
                OFS_FREQ = 4 * 0, ACT_POW = 4 * 1, REACT_POW = 4 * 2, IRMS = 4 * 3, URMS = 4 * 4,
                AVER_ACT = 4 * 5, AVER_REACT = 4 * 6, AVER_FREQ = 4 * 7, AVER_IRMS = 4 * 8, AVER_URMS = 4 * 9,
                UDC = 4 * 14, IDC = 4 * 15, ZXDATREG = 4 * 17, ZXDAT = 4 * 18, PHDAT = 4 * 19, T8BAUD = 4 * 21
            };

            public byte[] asRaw {
                set {
                    FREQINST = BitConverter.ToInt32(value, (int)regsOffsets.OFS_FREQ);
                    PAINST = BitConverter.ToInt32(value, (int)regsOffsets.ACT_POW);
                    QINST = BitConverter.ToInt32(value, (int)regsOffsets.REACT_POW);
                    IAINST = BitConverter.ToInt32(value, (int)regsOffsets.IRMS);
                    UINST = BitConverter.ToInt32(value, (int)regsOffsets.URMS);
                    PAAVG = BitConverter.ToInt32(value, (int)regsOffsets.AVER_ACT);
                    QAVG = BitConverter.ToInt32(value, (int)regsOffsets.AVER_REACT);
                    FREQAVG = BitConverter.ToInt32(value, (int)regsOffsets.AVER_FREQ);
                    IAAVG = BitConverter.ToInt32(value, (int)regsOffsets.AVER_IRMS);
                    UAVG = BitConverter.ToInt32(value, (int)regsOffsets.AVER_URMS);
                    UDCINST = BitConverter.ToInt32(value, (int)regsOffsets.UDC);
                    IADCINST = BitConverter.ToInt32(value, (int)regsOffsets.IDC);
                    ZXDATREG = BitConverter.ToInt32(value, (int)regsOffsets.ZXDATREG);
                    ZXDAT = BitConverter.ToInt32(value, (int)regsOffsets.ZXDAT);
                    PHDAT = BitConverter.ToInt32(value, (int)regsOffsets.PHDAT);
                    T8BAUD = BitConverter.ToInt32(value, (int)regsOffsets.T8BAUD);
                }
            }
        }


    }
}
