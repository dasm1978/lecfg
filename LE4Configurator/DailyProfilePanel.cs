﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;

using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class DailyProfilePanel : tabRDWRControl {
        public DailyProfilePanel() {
            InitializeComponent();
        }

        public override void OnRead(Protocol proto, Protocol.Commands cmdRead) {
            const int depth = 128;

            int timeDelta = 30 * 60; // must be readout

            byte[] arg = new byte[1];

            //byte[] reply = proto.sendPacketGetReply(Protocol.Query.ENQ, (byte)cmdRead, Protocol.MAX_REPLYLEN, arg, 3000);

            dataGridView1.RowTemplate.Height = 20;
            dataGridView1.ColumnCount = 3;
            dataGridView1.RowHeadersVisible = false;
            dataGridView1.Columns[0].HeaderText = "Номер";
            dataGridView1.Columns[1].HeaderText = "Время";
            dataGridView1.Columns[2].HeaderText = "Мощность, кВт";
            //dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;

            //dataGridView1.Columns[1].HeaderCell.Value = "Время";
            //dataGridView1.Columns[2].HeaderCell.Value = "Мощность, кВт";
            dataGridView1.Rows.Clear();
            chart1.Series.Clear();
            chart1.Series.Add("Мощность");
            chart1.Series["Мощность"].Points.Clear();
            

            byte[] replyIval = proto.sendPacketGetReply(Protocol.Query.ENQ, (byte)Protocol.Commands.RD_PROFILE_TIME, Protocol.MAX_REPLYLEN);
            if (replyIval == null)
                return;
            if (replyIval.Count() != 4)
                return;
            int ival = BitConverter.ToInt32(replyIval, 0);


            int ir = 0;
            List<Tuple<int, DateTime, double>> chartData = new List<Tuple<int, DateTime, double>>();
            for (byte index = 0; index < depth; index++) {
                arg[0] = index;
                byte[] reply = proto.sendPacketGetReply(Protocol.Query.ENQ, (byte)cmdRead, Protocol.MAX_REPLYLEN, arg, 3000);
                if (reply == null) {
                    MessageBox.Show("Не удалось считать");
                    break;
                }
                if (reply.Count() <= 5)
                    break;
                // reply fmt  min - hh - dd - mm -yy (low pwr, high pwr)
                int minute = (int)BCD.bcd2bin(reply[0]);
                int hh = (int)BCD.bcd2bin(reply[1]);
                int dd = (int)BCD.bcd2bin(reply[2]);
                int month = (int)BCD.bcd2bin(reply[3]);
                int year = (int)BCD.bcd2bin(reply[4]);

                DateTime dt = new DateTime(year + 2000, month + 1, dd, hh, minute, 0);
                 Int64 delta = ival * TimeSpan.TicksPerSecond; 
                


                byte[] data = new byte[256];
                Array.Copy(reply, 5, data, 0, reply.Count() - 5); // 5 date time len
                Array.Resize(ref data, reply.Count() - 5);


                double[] r = new double[data.Count() / 2];
                for (int i = 0; i < data.Count() / 2; i++) {
                    r[i] = (double)(BitConverter.ToUInt16(data, i * 2)) / 1000;
                }
                int packIdx = 0;
                foreach (double u in r) {                    
                    //dataGridView1.Rows.Add();                    
                    long ticks = dt.Ticks - (delta * packIdx);
                    DateTime dtp = new DateTime(ticks);
                    //dataGridView1.Rows[ir].Cells[0].Value = ir.ToString();
                    //dataGridView1.Rows[ir].Cells[1].Value = dtp.ToString("dd/MM/yyyy HH:mm");                    
                    //dataGridView1.Rows[ir].Cells[2].Value = String.Format("{0:0.00}", u);

                    // chartData.Add(new Tuple <string, string, string> (ir.ToString(), dtp.ToString("MM/dd/yyyy HH:mm"), u.ToString()));
                    chartData.Add(new Tuple<int, DateTime, double>(ir, dtp, u));
                    ir++;
                    packIdx++;
                }                
            }

            chartData.Reverse();

            for (int i = 1; i < chartData.Count; i++) {// skip first - erronuous 
                chart1.Series["Мощность"].Points.AddY(chartData[i].Item3);

                dataGridView1.Rows.Add();
                dataGridView1.Rows[i - 1].Cells[0].Value = i.ToString();
                dataGridView1.Rows[i - 1].Cells[1].Value =  chartData[i].Item2.ToString("dd/MM/yyyy HH:mm");
                dataGridView1.Rows[i - 1].Cells[2].Value = String.Format("{0:0.00}", chartData[i].Item3);
                //dtp.ToString("dd/MM/yyyy HH:mm");

            }

            dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            chart1.Invalidate();

        }
    }
}
