﻿namespace LE4Configurator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Параметры сети");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Вкл/Выкл. счетчика");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Снятия крышки счетчика");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Снятия крышки клеммной колодки");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Изменения даты и времени");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Воздействие магнитом");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Изменения тарифн. расписаний");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Изменения направления тока");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Превышения лимита мощности");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Превышения лимита энергии");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Журнал событий", new System.Windows.Forms.TreeNode[] {
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10});
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Текущие");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("По дням");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("По месяцам");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("По годам");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("Накопления", new System.Windows.Forms.TreeNode[] {
            treeNode12,
            treeNode13,
            treeNode14,
            treeNode15});
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("Суточные графики");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("Сезоны");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("Праздники");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("Тарифные расписания", new System.Windows.Forms.TreeNode[] {
            treeNode17,
            treeNode18,
            treeNode19});
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("Дата и время");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("Дисплей");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("Общие");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("Служебные");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("Настройки", new System.Windows.Forms.TreeNode[] {
            treeNode20,
            treeNode21,
            treeNode22,
            treeNode23,
            treeNode24});
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("Реле");
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("Профиль мощности");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("О счетчике");
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnStart = new System.Windows.Forms.ToolStripButton();
            this.btnStop = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cmbCOMPort = new System.Windows.Forms.ToolStripComboBox();
            this.btnRfsh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.comboUserAdmin = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.boxPass = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.boxNetAddr = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolBtnRead = new System.Windows.Forms.ToolStripButton();
            this.toolBtnWrite = new System.Windows.Forms.ToolStripButton();
            this.toolbtnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolBtnDel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolBtnFromFile = new System.Windows.Forms.ToolStripButton();
            this.toolBtnToFile = new System.Windows.Forms.ToolStripButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.tabAll = new System.Windows.Forms.TabControl();
            this.tabAbout = new System.Windows.Forms.TabPage();
            this.about1 = new LE4Configurator.About();
            this.tabEvents = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.treeEventsFilter = new System.Windows.Forms.TreeView();
            this.tabCoverEv = new System.Windows.Forms.TabPage();
            this.eventListCover = new LE4Configurator.eventList();
            this.dateTime = new System.Windows.Forms.TabPage();
            this.settingsPanel1 = new LE4Configurator.SettingsPanel();
            this.tabDailyGraphs = new System.Windows.Forms.TabPage();
            this.dailyPanel1 = new LE4Configurator.DailyPanel();
            this.tabSeasons = new System.Windows.Forms.TabPage();
            this.calendarPanel1 = new LE4Configurator.CalendarPanel();
            this.tabSpecDays = new System.Windows.Forms.TabPage();
            this.specDays1 = new LE4Configurator.SpecDays();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabDisplay = new System.Windows.Forms.TabPage();
            this.displaySettings1 = new LE4Configurator.DisplaySettings();
            this.tabDailyProfile = new System.Windows.Forms.TabPage();
            this.dailyProfilePanel1 = new LE4Configurator.DailyProfilePanel();
            this.tabDailyHistory = new System.Windows.Forms.TabPage();
            this.historyDays = new LE4Configurator.HistoryList();
            this.tabMonthlyHistory = new System.Windows.Forms.TabPage();
            this.historyMonth = new LE4Configurator.HistoryList();
            this.tabYearHistory = new System.Windows.Forms.TabPage();
            this.historyYear = new LE4Configurator.HistoryList();
            this.tabPowerEv = new System.Windows.Forms.TabPage();
            this.eventListPower = new LE4Configurator.eventList();
            this.tabMagnetEv = new System.Windows.Forms.TabPage();
            this.eventListMagnet = new LE4Configurator.eventList();
            this.tabTerminalEv = new System.Windows.Forms.TabPage();
            this.eventListTerminal = new LE4Configurator.eventList();
            this.tabDTChangeEv = new System.Windows.Forms.TabPage();
            this.eventListDTChanged = new LE4Configurator.eventList();
            this.tabTariffShegEv = new System.Windows.Forms.TabPage();
            this.eventListShedEv = new LE4Configurator.eventList();
            this.tabCurrChangeEv = new System.Windows.Forms.TabPage();
            this.eventListCurrChanged = new LE4Configurator.eventList();
            this.tabEnLimEv = new System.Windows.Forms.TabPage();
            this.eventListEnLim = new LE4Configurator.eventList();
            this.tabPowLimEv = new System.Windows.Forms.TabPage();
            this.eventListPowLim = new LE4Configurator.eventList();
            this.tabRelay = new System.Windows.Forms.TabPage();
            this.relay1 = new LE4Configurator.Relay();
            this.tabCurrentEnergy = new System.Windows.Forms.TabPage();
            this.currentEnergy1 = new LE4Configurator.CurrentEnergy();
            this.tabService = new System.Windows.Forms.TabPage();
            this.servicePanel1 = new LE4Configurator.ServicePanel();
            this.tabCommon = new System.Windows.Forms.TabPage();
            this.commonSettings1 = new LE4Configurator.CommonSettings();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabAll.SuspendLayout();
            this.tabAbout.SuspendLayout();
            this.tabEvents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tabCoverEv.SuspendLayout();
            this.dateTime.SuspendLayout();
            this.tabDailyGraphs.SuspendLayout();
            this.tabSeasons.SuspendLayout();
            this.tabSpecDays.SuspendLayout();
            this.tabDisplay.SuspendLayout();
            this.tabDailyProfile.SuspendLayout();
            this.tabDailyHistory.SuspendLayout();
            this.tabMonthlyHistory.SuspendLayout();
            this.tabYearHistory.SuspendLayout();
            this.tabPowerEv.SuspendLayout();
            this.tabMagnetEv.SuspendLayout();
            this.tabTerminalEv.SuspendLayout();
            this.tabDTChangeEv.SuspendLayout();
            this.tabTariffShegEv.SuspendLayout();
            this.tabCurrChangeEv.SuspendLayout();
            this.tabEnLimEv.SuspendLayout();
            this.tabPowLimEv.SuspendLayout();
            this.tabRelay.SuspendLayout();
            this.tabCurrentEnergy.SuspendLayout();
            this.tabService.SuspendLayout();
            this.tabCommon.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Location = new System.Drawing.Point(0, 668);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1484, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "---";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnStart,
            this.btnStop,
            this.toolStripSeparator2,
            this.toolStripLabel1,
            this.cmbCOMPort,
            this.btnRfsh,
            this.toolStripSeparator1,
            this.toolStripLabel2,
            this.comboUserAdmin,
            this.toolStripSeparator3,
            this.toolStripLabel3,
            this.boxPass,
            this.toolStripSeparator4,
            this.toolStripLabel4,
            this.boxNetAddr,
            this.toolStripSeparator5,
            this.toolStripSeparator7,
            this.toolBtnRead,
            this.toolBtnWrite,
            this.toolbtnAdd,
            this.toolBtnDel,
            this.toolStripSeparator6,
            this.toolBtnFromFile,
            this.toolBtnToFile});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1484, 31);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnStart
            // 
            this.btnStart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(28, 28);
            this.btnStart.Text = "Соединение";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnStop.Image = ((System.Drawing.Image)(resources.GetObject("btnStop.Image")));
            this.btnStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(28, 28);
            this.btnStop.Text = "Остановить";
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(67, 28);
            this.toolStripLabel1.Text = "COM порт ";
            // 
            // cmbCOMPort
            // 
            this.cmbCOMPort.Name = "cmbCOMPort";
            this.cmbCOMPort.Size = new System.Drawing.Size(121, 31);
            // 
            // btnRfsh
            // 
            this.btnRfsh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRfsh.Image = ((System.Drawing.Image)(resources.GetObject("btnRfsh.Image")));
            this.btnRfsh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRfsh.Name = "btnRfsh";
            this.btnRfsh.Size = new System.Drawing.Size(28, 28);
            this.btnRfsh.Text = "Обновить порты";
            this.btnRfsh.Click += new System.EventHandler(this.btnRfsh_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(41, 28);
            this.toolStripLabel2.Text = "Логин";
            // 
            // comboUserAdmin
            // 
            this.comboUserAdmin.Items.AddRange(new object[] {
            "Пользователь",
            "Администратор"});
            this.comboUserAdmin.Name = "comboUserAdmin";
            this.comboUserAdmin.Size = new System.Drawing.Size(121, 31);
            this.comboUserAdmin.Click += new System.EventHandler(this.comboUserAdmin_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(49, 28);
            this.toolStripLabel3.Text = "Пароль";
            this.toolStripLabel3.Click += new System.EventHandler(this.toolStripLabel3_Click);
            // 
            // boxPass
            // 
            this.boxPass.Name = "boxPass";
            this.boxPass.Size = new System.Drawing.Size(100, 31);
            this.boxPass.Enter += new System.EventHandler(this.boxPass_Enter);
            this.boxPass.Click += new System.EventHandler(this.boxPass_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(63, 28);
            this.toolStripLabel4.Text = "Сет. адрес";
            // 
            // boxNetAddr
            // 
            this.boxNetAddr.Name = "boxNetAddr";
            this.boxNetAddr.Size = new System.Drawing.Size(100, 31);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 31);
            // 
            // toolBtnRead
            // 
            this.toolBtnRead.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnRead.Image")));
            this.toolBtnRead.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnRead.Name = "toolBtnRead";
            this.toolBtnRead.RightToLeftAutoMirrorImage = true;
            this.toolBtnRead.Size = new System.Drawing.Size(74, 28);
            this.toolBtnRead.Text = "Чтение";
            this.toolBtnRead.Click += new System.EventHandler(this.toolBtnRead_Click);
            // 
            // toolBtnWrite
            // 
            this.toolBtnWrite.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnWrite.Image")));
            this.toolBtnWrite.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnWrite.Name = "toolBtnWrite";
            this.toolBtnWrite.Size = new System.Drawing.Size(74, 28);
            this.toolBtnWrite.Text = "Запись";
            this.toolBtnWrite.Click += new System.EventHandler(this.toolBtnWrite_Click);
            // 
            // toolbtnAdd
            // 
            this.toolbtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolbtnAdd.Image")));
            this.toolbtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtnAdd.Name = "toolbtnAdd";
            this.toolbtnAdd.Size = new System.Drawing.Size(87, 28);
            this.toolbtnAdd.Text = "Добавить";
            this.toolbtnAdd.Click += new System.EventHandler(this.toolbtnAdd_Click);
            // 
            // toolBtnDel
            // 
            this.toolBtnDel.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnDel.Image")));
            this.toolBtnDel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnDel.Name = "toolBtnDel";
            this.toolBtnDel.Size = new System.Drawing.Size(79, 28);
            this.toolBtnDel.Text = "Удалить";
            this.toolBtnDel.Click += new System.EventHandler(this.toolBtnDel_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 31);
            // 
            // toolBtnFromFile
            // 
            this.toolBtnFromFile.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnFromFile.Image")));
            this.toolBtnFromFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnFromFile.Name = "toolBtnFromFile";
            this.toolBtnFromFile.Size = new System.Drawing.Size(87, 28);
            this.toolBtnFromFile.Text = "Из файла";
            this.toolBtnFromFile.Click += new System.EventHandler(this.toolBtnFromFile_Click);
            // 
            // toolBtnToFile
            // 
            this.toolBtnToFile.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnToFile.Image")));
            this.toolBtnToFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnToFile.Name = "toolBtnToFile";
            this.toolBtnToFile.Size = new System.Drawing.Size(74, 28);
            this.toolBtnToFile.Text = "В Файл";
            this.toolBtnToFile.Click += new System.EventHandler(this.toolBtnToFile_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "stop.png");
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 31);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabAll);
            this.splitContainer1.Size = new System.Drawing.Size(1484, 637);
            this.splitContainer1.SplitterDistance = 374;
            this.splitContainer1.TabIndex = 2;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "measure";
            treeNode1.Text = "Параметры сети";
            treeNode2.Name = "powOnOff";
            treeNode2.Text = "Вкл/Выкл. счетчика";
            treeNode3.Name = "Снятия крышки счетчика";
            treeNode3.Text = "Снятия крышки счетчика";
            treeNode4.Name = "coverOn";
            treeNode4.Text = "Снятия крышки клеммной колодки";
            treeNode5.Name = "dtChange";
            treeNode5.Text = "Изменения даты и времени";
            treeNode6.Name = "magnet";
            treeNode6.Text = "Воздействие магнитом";
            treeNode7.Name = "tariffChange";
            treeNode7.Text = "Изменения тарифн. расписаний";
            treeNode8.Name = "currentRev";
            treeNode8.Text = "Изменения направления тока";
            treeNode9.Name = "limitPower";
            treeNode9.Text = "Превышения лимита мощности";
            treeNode10.Name = "limitEnergy";
            treeNode10.Text = "Превышения лимита энергии";
            treeNode11.Name = "eventLog";
            treeNode11.Text = "Журнал событий";
            treeNode12.Name = "Текущие";
            treeNode12.Text = "Текущие";
            treeNode13.Name = "byDays";
            treeNode13.Text = "По дням";
            treeNode14.BackColor = System.Drawing.Color.White;
            treeNode14.Name = "byMon";
            treeNode14.Text = "По месяцам";
            treeNode15.Name = "byYear";
            treeNode15.Text = "По годам";
            treeNode16.Name = "itogs";
            treeNode16.Text = "Накопления";
            treeNode17.Name = "dailyGraph";
            treeNode17.Text = "Суточные графики";
            treeNode18.Name = "seasons";
            treeNode18.Text = "Сезоны";
            treeNode19.Name = "holidays";
            treeNode19.Text = "Праздники";
            treeNode20.Name = "Node1";
            treeNode20.Text = "Тарифные расписания";
            treeNode21.Name = "dateTime";
            treeNode21.Text = "Дата и время";
            treeNode22.Name = "nodeDisp";
            treeNode22.Text = "Дисплей";
            treeNode23.Name = "Общие";
            treeNode23.Text = "Общие";
            treeNode24.Name = "Node1";
            treeNode24.Text = "Служебные";
            treeNode25.Name = "Settings";
            treeNode25.Text = "Настройки";
            treeNode26.Name = "Relay";
            treeNode26.Text = "Реле";
            treeNode27.Name = "powerProf";
            treeNode27.Text = "Профиль мощности";
            treeNode28.Name = "about";
            treeNode28.Text = "О счетчике";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode11,
            treeNode16,
            treeNode25,
            treeNode26,
            treeNode27,
            treeNode28});
            this.treeView1.Size = new System.Drawing.Size(374, 637);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // tabAll
            // 
            this.tabAll.Controls.Add(this.tabAbout);
            this.tabAll.Controls.Add(this.tabEvents);
            this.tabAll.Controls.Add(this.tabCoverEv);
            this.tabAll.Controls.Add(this.dateTime);
            this.tabAll.Controls.Add(this.tabDailyGraphs);
            this.tabAll.Controls.Add(this.tabSeasons);
            this.tabAll.Controls.Add(this.tabSpecDays);
            this.tabAll.Controls.Add(this.tabPage1);
            this.tabAll.Controls.Add(this.tabDisplay);
            this.tabAll.Controls.Add(this.tabDailyProfile);
            this.tabAll.Controls.Add(this.tabDailyHistory);
            this.tabAll.Controls.Add(this.tabMonthlyHistory);
            this.tabAll.Controls.Add(this.tabYearHistory);
            this.tabAll.Controls.Add(this.tabPowerEv);
            this.tabAll.Controls.Add(this.tabMagnetEv);
            this.tabAll.Controls.Add(this.tabTerminalEv);
            this.tabAll.Controls.Add(this.tabDTChangeEv);
            this.tabAll.Controls.Add(this.tabTariffShegEv);
            this.tabAll.Controls.Add(this.tabCurrChangeEv);
            this.tabAll.Controls.Add(this.tabEnLimEv);
            this.tabAll.Controls.Add(this.tabPowLimEv);
            this.tabAll.Controls.Add(this.tabRelay);
            this.tabAll.Controls.Add(this.tabCurrentEnergy);
            this.tabAll.Controls.Add(this.tabService);
            this.tabAll.Controls.Add(this.tabCommon);
            this.tabAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabAll.Location = new System.Drawing.Point(0, 0);
            this.tabAll.Name = "tabAll";
            this.tabAll.SelectedIndex = 0;
            this.tabAll.Size = new System.Drawing.Size(1106, 637);
            this.tabAll.TabIndex = 0;
            // 
            // tabAbout
            // 
            this.tabAbout.Controls.Add(this.about1);
            this.tabAbout.Location = new System.Drawing.Point(4, 22);
            this.tabAbout.Name = "tabAbout";
            this.tabAbout.Padding = new System.Windows.Forms.Padding(3);
            this.tabAbout.Size = new System.Drawing.Size(1098, 611);
            this.tabAbout.TabIndex = 0;
            this.tabAbout.Text = "tabAbout";
            this.tabAbout.UseVisualStyleBackColor = true;
            // 
            // about1
            // 
            this.about1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.about1.Location = new System.Drawing.Point(3, 3);
            this.about1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.about1.Name = "about1";
            this.about1.Size = new System.Drawing.Size(1092, 605);
            this.about1.TabIndex = 0;
            this.about1.Load += new System.EventHandler(this.about1_Load);
            // 
            // tabEvents
            // 
            this.tabEvents.Controls.Add(this.splitContainer2);
            this.tabEvents.Location = new System.Drawing.Point(4, 22);
            this.tabEvents.Name = "tabEvents";
            this.tabEvents.Padding = new System.Windows.Forms.Padding(3);
            this.tabEvents.Size = new System.Drawing.Size(1098, 611);
            this.tabEvents.TabIndex = 1;
            this.tabEvents.Text = "jrnEvents";
            this.tabEvents.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.treeEventsFilter);
            this.splitContainer2.Size = new System.Drawing.Size(1092, 605);
            this.splitContainer2.SplitterDistance = 324;
            this.splitContainer2.TabIndex = 0;
            // 
            // treeEventsFilter
            // 
            this.treeEventsFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeEventsFilter.Location = new System.Drawing.Point(0, 0);
            this.treeEventsFilter.Name = "treeEventsFilter";
            this.treeEventsFilter.Size = new System.Drawing.Size(324, 605);
            this.treeEventsFilter.TabIndex = 0;
            // 
            // tabCoverEv
            // 
            this.tabCoverEv.Controls.Add(this.eventListCover);
            this.tabCoverEv.Location = new System.Drawing.Point(4, 22);
            this.tabCoverEv.Name = "tabCoverEv";
            this.tabCoverEv.Size = new System.Drawing.Size(1098, 611);
            this.tabCoverEv.TabIndex = 2;
            this.tabCoverEv.Text = "tabCoverEv";
            this.tabCoverEv.UseVisualStyleBackColor = true;
            // 
            // eventListCover
            // 
            this.eventListCover.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventListCover.Location = new System.Drawing.Point(0, 0);
            this.eventListCover.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eventListCover.Name = "eventListCover";
            this.eventListCover.Size = new System.Drawing.Size(1098, 611);
            this.eventListCover.TabIndex = 0;
            // 
            // dateTime
            // 
            this.dateTime.BackColor = System.Drawing.Color.Transparent;
            this.dateTime.Controls.Add(this.settingsPanel1);
            this.dateTime.Location = new System.Drawing.Point(4, 22);
            this.dateTime.Name = "dateTime";
            this.dateTime.Size = new System.Drawing.Size(1098, 611);
            this.dateTime.TabIndex = 3;
            this.dateTime.Text = "tabSettings";
            // 
            // settingsPanel1
            // 
            this.settingsPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settingsPanel1.Location = new System.Drawing.Point(0, 0);
            this.settingsPanel1.Margin = new System.Windows.Forms.Padding(1);
            this.settingsPanel1.Name = "settingsPanel1";
            this.settingsPanel1.Size = new System.Drawing.Size(1098, 611);
            this.settingsPanel1.TabIndex = 0;
            this.settingsPanel1.Load += new System.EventHandler(this.settingsPanel1_Load);
            // 
            // tabDailyGraphs
            // 
            this.tabDailyGraphs.Controls.Add(this.dailyPanel1);
            this.tabDailyGraphs.Location = new System.Drawing.Point(4, 22);
            this.tabDailyGraphs.Margin = new System.Windows.Forms.Padding(2);
            this.tabDailyGraphs.Name = "tabDailyGraphs";
            this.tabDailyGraphs.Size = new System.Drawing.Size(1098, 611);
            this.tabDailyGraphs.TabIndex = 4;
            this.tabDailyGraphs.Text = "tabDailyGraphs";
            this.tabDailyGraphs.UseVisualStyleBackColor = true;
            // 
            // dailyPanel1
            // 
            this.dailyPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dailyPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dailyPanel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.dailyPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dailyPanel1.Location = new System.Drawing.Point(0, 0);
            this.dailyPanel1.Margin = new System.Windows.Forms.Padding(1);
            this.dailyPanel1.Name = "dailyPanel1";
            this.dailyPanel1.Size = new System.Drawing.Size(1098, 611);
            this.dailyPanel1.TabIndex = 17;
            // 
            // tabSeasons
            // 
            this.tabSeasons.Controls.Add(this.calendarPanel1);
            this.tabSeasons.Location = new System.Drawing.Point(4, 22);
            this.tabSeasons.Margin = new System.Windows.Forms.Padding(2);
            this.tabSeasons.Name = "tabSeasons";
            this.tabSeasons.Size = new System.Drawing.Size(1098, 611);
            this.tabSeasons.TabIndex = 5;
            this.tabSeasons.Text = "tabSeasons";
            this.tabSeasons.UseVisualStyleBackColor = true;
            // 
            // calendarPanel1
            // 
            this.calendarPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.calendarPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarPanel1.Location = new System.Drawing.Point(0, 0);
            this.calendarPanel1.Margin = new System.Windows.Forms.Padding(1);
            this.calendarPanel1.Name = "calendarPanel1";
            this.calendarPanel1.Size = new System.Drawing.Size(1098, 611);
            this.calendarPanel1.TabIndex = 5;
            // 
            // tabSpecDays
            // 
            this.tabSpecDays.Controls.Add(this.specDays1);
            this.tabSpecDays.Location = new System.Drawing.Point(4, 22);
            this.tabSpecDays.Margin = new System.Windows.Forms.Padding(2);
            this.tabSpecDays.Name = "tabSpecDays";
            this.tabSpecDays.Size = new System.Drawing.Size(1098, 611);
            this.tabSpecDays.TabIndex = 21;
            this.tabSpecDays.Text = "tabSpecDays";
            this.tabSpecDays.UseVisualStyleBackColor = true;
            // 
            // specDays1
            // 
            this.specDays1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.specDays1.Location = new System.Drawing.Point(0, 0);
            this.specDays1.Margin = new System.Windows.Forms.Padding(1);
            this.specDays1.Name = "specDays1";
            this.specDays1.Size = new System.Drawing.Size(1098, 611);
            this.specDays1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1098, 611);
            this.tabPage1.TabIndex = 6;
            this.tabPage1.Text = "xxx";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabDisplay
            // 
            this.tabDisplay.Controls.Add(this.displaySettings1);
            this.tabDisplay.Location = new System.Drawing.Point(4, 22);
            this.tabDisplay.Name = "tabDisplay";
            this.tabDisplay.Size = new System.Drawing.Size(1098, 611);
            this.tabDisplay.TabIndex = 7;
            this.tabDisplay.Text = "tabDisplay";
            this.tabDisplay.UseVisualStyleBackColor = true;
            // 
            // displaySettings1
            // 
            this.displaySettings1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.displaySettings1.Location = new System.Drawing.Point(0, 0);
            this.displaySettings1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.displaySettings1.Name = "displaySettings1";
            this.displaySettings1.Size = new System.Drawing.Size(1098, 611);
            this.displaySettings1.TabIndex = 0;
            // 
            // tabDailyProfile
            // 
            this.tabDailyProfile.Controls.Add(this.dailyProfilePanel1);
            this.tabDailyProfile.Location = new System.Drawing.Point(4, 22);
            this.tabDailyProfile.Name = "tabDailyProfile";
            this.tabDailyProfile.Size = new System.Drawing.Size(1098, 611);
            this.tabDailyProfile.TabIndex = 8;
            this.tabDailyProfile.Text = "tabDailyProfile";
            this.tabDailyProfile.UseVisualStyleBackColor = true;
            // 
            // dailyProfilePanel1
            // 
            this.dailyProfilePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dailyProfilePanel1.Location = new System.Drawing.Point(0, 0);
            this.dailyProfilePanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dailyProfilePanel1.Name = "dailyProfilePanel1";
            this.dailyProfilePanel1.Size = new System.Drawing.Size(1098, 611);
            this.dailyProfilePanel1.TabIndex = 0;
            // 
            // tabDailyHistory
            // 
            this.tabDailyHistory.Controls.Add(this.historyDays);
            this.tabDailyHistory.Location = new System.Drawing.Point(4, 22);
            this.tabDailyHistory.Margin = new System.Windows.Forms.Padding(2);
            this.tabDailyHistory.Name = "tabDailyHistory";
            this.tabDailyHistory.Size = new System.Drawing.Size(1098, 611);
            this.tabDailyHistory.TabIndex = 9;
            this.tabDailyHistory.Text = "tabDailyHistory";
            this.tabDailyHistory.UseVisualStyleBackColor = true;
            // 
            // historyDays
            // 
            this.historyDays.Dock = System.Windows.Forms.DockStyle.Fill;
            this.historyDays.Location = new System.Drawing.Point(0, 0);
            this.historyDays.Margin = new System.Windows.Forms.Padding(1);
            this.historyDays.Name = "historyDays";
            this.historyDays.Size = new System.Drawing.Size(1098, 611);
            this.historyDays.TabIndex = 0;
            this.historyDays.Load += new System.EventHandler(this.historyList1_Load);
            // 
            // tabMonthlyHistory
            // 
            this.tabMonthlyHistory.Controls.Add(this.historyMonth);
            this.tabMonthlyHistory.Location = new System.Drawing.Point(4, 22);
            this.tabMonthlyHistory.Margin = new System.Windows.Forms.Padding(2);
            this.tabMonthlyHistory.Name = "tabMonthlyHistory";
            this.tabMonthlyHistory.Size = new System.Drawing.Size(1098, 611);
            this.tabMonthlyHistory.TabIndex = 10;
            this.tabMonthlyHistory.Text = "tabMonthlyHistory";
            this.tabMonthlyHistory.UseVisualStyleBackColor = true;
            // 
            // historyMonth
            // 
            this.historyMonth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.historyMonth.Location = new System.Drawing.Point(0, 0);
            this.historyMonth.Margin = new System.Windows.Forms.Padding(1);
            this.historyMonth.Name = "historyMonth";
            this.historyMonth.Size = new System.Drawing.Size(1098, 611);
            this.historyMonth.TabIndex = 0;
            // 
            // tabYearHistory
            // 
            this.tabYearHistory.Controls.Add(this.historyYear);
            this.tabYearHistory.Location = new System.Drawing.Point(4, 22);
            this.tabYearHistory.Margin = new System.Windows.Forms.Padding(2);
            this.tabYearHistory.Name = "tabYearHistory";
            this.tabYearHistory.Size = new System.Drawing.Size(1098, 611);
            this.tabYearHistory.TabIndex = 11;
            this.tabYearHistory.Text = "tabYearHistory";
            this.tabYearHistory.UseVisualStyleBackColor = true;
            // 
            // historyYear
            // 
            this.historyYear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.historyYear.Location = new System.Drawing.Point(0, 0);
            this.historyYear.Margin = new System.Windows.Forms.Padding(1);
            this.historyYear.Name = "historyYear";
            this.historyYear.Size = new System.Drawing.Size(1098, 611);
            this.historyYear.TabIndex = 0;
            // 
            // tabPowerEv
            // 
            this.tabPowerEv.Controls.Add(this.eventListPower);
            this.tabPowerEv.Location = new System.Drawing.Point(4, 22);
            this.tabPowerEv.Name = "tabPowerEv";
            this.tabPowerEv.Size = new System.Drawing.Size(1098, 611);
            this.tabPowerEv.TabIndex = 12;
            this.tabPowerEv.Text = "tabPowerEv";
            this.tabPowerEv.UseVisualStyleBackColor = true;
            // 
            // eventListPower
            // 
            this.eventListPower.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventListPower.Location = new System.Drawing.Point(0, 0);
            this.eventListPower.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eventListPower.Name = "eventListPower";
            this.eventListPower.Size = new System.Drawing.Size(1098, 611);
            this.eventListPower.TabIndex = 0;
            // 
            // tabMagnetEv
            // 
            this.tabMagnetEv.Controls.Add(this.eventListMagnet);
            this.tabMagnetEv.Location = new System.Drawing.Point(4, 22);
            this.tabMagnetEv.Name = "tabMagnetEv";
            this.tabMagnetEv.Size = new System.Drawing.Size(1098, 611);
            this.tabMagnetEv.TabIndex = 13;
            this.tabMagnetEv.Text = "tabMagnetEv";
            this.tabMagnetEv.UseVisualStyleBackColor = true;
            // 
            // eventListMagnet
            // 
            this.eventListMagnet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventListMagnet.Location = new System.Drawing.Point(0, 0);
            this.eventListMagnet.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eventListMagnet.Name = "eventListMagnet";
            this.eventListMagnet.Size = new System.Drawing.Size(1098, 611);
            this.eventListMagnet.TabIndex = 0;
            // 
            // tabTerminalEv
            // 
            this.tabTerminalEv.Controls.Add(this.eventListTerminal);
            this.tabTerminalEv.Location = new System.Drawing.Point(4, 22);
            this.tabTerminalEv.Name = "tabTerminalEv";
            this.tabTerminalEv.Size = new System.Drawing.Size(1098, 611);
            this.tabTerminalEv.TabIndex = 14;
            this.tabTerminalEv.Text = "tabTerminalEv";
            this.tabTerminalEv.UseVisualStyleBackColor = true;
            // 
            // eventListTerminal
            // 
            this.eventListTerminal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventListTerminal.Location = new System.Drawing.Point(0, 0);
            this.eventListTerminal.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eventListTerminal.Name = "eventListTerminal";
            this.eventListTerminal.Size = new System.Drawing.Size(1098, 611);
            this.eventListTerminal.TabIndex = 0;
            // 
            // tabDTChangeEv
            // 
            this.tabDTChangeEv.Controls.Add(this.eventListDTChanged);
            this.tabDTChangeEv.Location = new System.Drawing.Point(4, 22);
            this.tabDTChangeEv.Name = "tabDTChangeEv";
            this.tabDTChangeEv.Size = new System.Drawing.Size(1098, 611);
            this.tabDTChangeEv.TabIndex = 15;
            this.tabDTChangeEv.Text = "tabDTChangeEv";
            this.tabDTChangeEv.UseVisualStyleBackColor = true;
            // 
            // eventListDTChanged
            // 
            this.eventListDTChanged.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventListDTChanged.Location = new System.Drawing.Point(0, 0);
            this.eventListDTChanged.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eventListDTChanged.Name = "eventListDTChanged";
            this.eventListDTChanged.Size = new System.Drawing.Size(1098, 611);
            this.eventListDTChanged.TabIndex = 0;
            // 
            // tabTariffShegEv
            // 
            this.tabTariffShegEv.Controls.Add(this.eventListShedEv);
            this.tabTariffShegEv.Location = new System.Drawing.Point(4, 22);
            this.tabTariffShegEv.Name = "tabTariffShegEv";
            this.tabTariffShegEv.Size = new System.Drawing.Size(1098, 611);
            this.tabTariffShegEv.TabIndex = 16;
            this.tabTariffShegEv.Text = "tabTariffShegEv";
            this.tabTariffShegEv.UseVisualStyleBackColor = true;
            // 
            // eventListShedEv
            // 
            this.eventListShedEv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventListShedEv.Location = new System.Drawing.Point(0, 0);
            this.eventListShedEv.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eventListShedEv.Name = "eventListShedEv";
            this.eventListShedEv.Size = new System.Drawing.Size(1098, 611);
            this.eventListShedEv.TabIndex = 0;
            // 
            // tabCurrChangeEv
            // 
            this.tabCurrChangeEv.Controls.Add(this.eventListCurrChanged);
            this.tabCurrChangeEv.Location = new System.Drawing.Point(4, 22);
            this.tabCurrChangeEv.Name = "tabCurrChangeEv";
            this.tabCurrChangeEv.Size = new System.Drawing.Size(1098, 611);
            this.tabCurrChangeEv.TabIndex = 17;
            this.tabCurrChangeEv.Text = "tabCurrChangeEv";
            this.tabCurrChangeEv.UseVisualStyleBackColor = true;
            // 
            // eventListCurrChanged
            // 
            this.eventListCurrChanged.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventListCurrChanged.Location = new System.Drawing.Point(0, 0);
            this.eventListCurrChanged.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eventListCurrChanged.Name = "eventListCurrChanged";
            this.eventListCurrChanged.Size = new System.Drawing.Size(1098, 611);
            this.eventListCurrChanged.TabIndex = 0;
            // 
            // tabEnLimEv
            // 
            this.tabEnLimEv.Controls.Add(this.eventListEnLim);
            this.tabEnLimEv.Location = new System.Drawing.Point(4, 22);
            this.tabEnLimEv.Name = "tabEnLimEv";
            this.tabEnLimEv.Size = new System.Drawing.Size(1098, 611);
            this.tabEnLimEv.TabIndex = 18;
            this.tabEnLimEv.Text = "tabEnLimEv";
            this.tabEnLimEv.UseVisualStyleBackColor = true;
            // 
            // eventListEnLim
            // 
            this.eventListEnLim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventListEnLim.Location = new System.Drawing.Point(0, 0);
            this.eventListEnLim.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eventListEnLim.Name = "eventListEnLim";
            this.eventListEnLim.Size = new System.Drawing.Size(1098, 611);
            this.eventListEnLim.TabIndex = 0;
            // 
            // tabPowLimEv
            // 
            this.tabPowLimEv.Controls.Add(this.eventListPowLim);
            this.tabPowLimEv.Location = new System.Drawing.Point(4, 22);
            this.tabPowLimEv.Name = "tabPowLimEv";
            this.tabPowLimEv.Size = new System.Drawing.Size(1098, 611);
            this.tabPowLimEv.TabIndex = 19;
            this.tabPowLimEv.Text = "tabPowLimEv";
            this.tabPowLimEv.UseVisualStyleBackColor = true;
            // 
            // eventListPowLim
            // 
            this.eventListPowLim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventListPowLim.Location = new System.Drawing.Point(0, 0);
            this.eventListPowLim.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eventListPowLim.Name = "eventListPowLim";
            this.eventListPowLim.Size = new System.Drawing.Size(1098, 611);
            this.eventListPowLim.TabIndex = 0;
            // 
            // tabRelay
            // 
            this.tabRelay.Controls.Add(this.relay1);
            this.tabRelay.Location = new System.Drawing.Point(4, 22);
            this.tabRelay.Name = "tabRelay";
            this.tabRelay.Size = new System.Drawing.Size(1098, 611);
            this.tabRelay.TabIndex = 20;
            this.tabRelay.Text = "tabRelay";
            this.tabRelay.UseVisualStyleBackColor = true;
            // 
            // relay1
            // 
            this.relay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.relay1.Location = new System.Drawing.Point(0, 0);
            this.relay1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.relay1.Name = "relay1";
            this.relay1.Size = new System.Drawing.Size(1098, 611);
            this.relay1.TabIndex = 0;
            // 
            // tabCurrentEnergy
            // 
            this.tabCurrentEnergy.Controls.Add(this.currentEnergy1);
            this.tabCurrentEnergy.Location = new System.Drawing.Point(4, 22);
            this.tabCurrentEnergy.Margin = new System.Windows.Forms.Padding(2);
            this.tabCurrentEnergy.Name = "tabCurrentEnergy";
            this.tabCurrentEnergy.Size = new System.Drawing.Size(1098, 611);
            this.tabCurrentEnergy.TabIndex = 22;
            this.tabCurrentEnergy.Text = "tabCurrentEnergy";
            this.tabCurrentEnergy.UseVisualStyleBackColor = true;
            // 
            // currentEnergy1
            // 
            this.currentEnergy1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.currentEnergy1.Location = new System.Drawing.Point(0, 0);
            this.currentEnergy1.Margin = new System.Windows.Forms.Padding(1);
            this.currentEnergy1.Name = "currentEnergy1";
            this.currentEnergy1.Size = new System.Drawing.Size(1098, 611);
            this.currentEnergy1.TabIndex = 0;
            this.currentEnergy1.Load += new System.EventHandler(this.currentEnergy1_Load);
            // 
            // tabService
            // 
            this.tabService.Controls.Add(this.servicePanel1);
            this.tabService.Location = new System.Drawing.Point(4, 22);
            this.tabService.Name = "tabService";
            this.tabService.Size = new System.Drawing.Size(1098, 611);
            this.tabService.TabIndex = 23;
            this.tabService.Text = "Служебные";
            this.tabService.UseVisualStyleBackColor = true;
            // 
            // servicePanel1
            // 
            this.servicePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.servicePanel1.Location = new System.Drawing.Point(0, 0);
            this.servicePanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.servicePanel1.Name = "servicePanel1";
            this.servicePanel1.Size = new System.Drawing.Size(1098, 611);
            this.servicePanel1.TabIndex = 0;
            // 
            // tabCommon
            // 
            this.tabCommon.Controls.Add(this.commonSettings1);
            this.tabCommon.Location = new System.Drawing.Point(4, 22);
            this.tabCommon.Name = "tabCommon";
            this.tabCommon.Size = new System.Drawing.Size(1098, 611);
            this.tabCommon.TabIndex = 24;
            this.tabCommon.Text = "tabCommon";
            this.tabCommon.UseVisualStyleBackColor = true;
            // 
            // commonSettings1
            // 
            this.commonSettings1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.commonSettings1.Location = new System.Drawing.Point(0, 0);
            this.commonSettings1.Name = "commonSettings1";
            this.commonSettings1.Size = new System.Drawing.Size(1098, 611);
            this.commonSettings1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1484, 690);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "Form1";
            this.Text = "LE4";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabAll.ResumeLayout(false);
            this.tabAbout.ResumeLayout(false);
            this.tabEvents.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tabCoverEv.ResumeLayout(false);
            this.dateTime.ResumeLayout(false);
            this.tabDailyGraphs.ResumeLayout(false);
            this.tabSeasons.ResumeLayout(false);
            this.tabSpecDays.ResumeLayout(false);
            this.tabDisplay.ResumeLayout(false);
            this.tabDailyProfile.ResumeLayout(false);
            this.tabDailyHistory.ResumeLayout(false);
            this.tabMonthlyHistory.ResumeLayout(false);
            this.tabYearHistory.ResumeLayout(false);
            this.tabPowerEv.ResumeLayout(false);
            this.tabMagnetEv.ResumeLayout(false);
            this.tabTerminalEv.ResumeLayout(false);
            this.tabDTChangeEv.ResumeLayout(false);
            this.tabTariffShegEv.ResumeLayout(false);
            this.tabCurrChangeEv.ResumeLayout(false);
            this.tabEnLimEv.ResumeLayout(false);
            this.tabPowLimEv.ResumeLayout(false);
            this.tabRelay.ResumeLayout(false);
            this.tabCurrentEnergy.ResumeLayout(false);
            this.tabService.ResumeLayout(false);
            this.tabCommon.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ToolStripButton btnStop;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripButton btnStart;
        private System.Windows.Forms.ToolStripComboBox cmbCOMPort;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox comboUserAdmin;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripButton btnRfsh;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TabControl tabAll;
        private System.Windows.Forms.TabPage tabAbout;
        private System.Windows.Forms.TabPage tabEvents;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TreeView treeEventsFilter;
        private System.Windows.Forms.TabPage tabCoverEv;
        private System.Windows.Forms.TabPage dateTime;
        private SettingsPanel settingsPanel1;
        private System.Windows.Forms.TabPage tabDailyGraphs;
        private System.Windows.Forms.TabPage tabSeasons;
        private System.Windows.Forms.TabPage tabPage1;
        private DailyPanel dailyPanel1;        
        private CalendarPanel calendarPanel1;
        private System.Windows.Forms.ToolStripTextBox boxPass;
        private System.Windows.Forms.TabPage tabDisplay;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolBtnRead;
        private System.Windows.Forms.TabPage tabDailyProfile;
        private DailyProfilePanel dailyProfilePanel1;
        private System.Windows.Forms.TabPage tabDailyHistory;
        private HistoryList historyDays;
        private System.Windows.Forms.TabPage tabMonthlyHistory;
        private HistoryList historyMonth;
        private System.Windows.Forms.TabPage tabYearHistory;
        private HistoryList historyYear;
        private System.Windows.Forms.ToolStripButton toolBtnWrite;
        private DisplaySettings displaySettings1;
        private System.Windows.Forms.TabPage tabPowerEv;
        private System.Windows.Forms.TabPage tabMagnetEv;
        private System.Windows.Forms.TabPage tabTerminalEv;
        private eventList eventListPower;
        private eventList eventListMagnet;
        private eventList eventListTerminal;
        private System.Windows.Forms.TabPage tabDTChangeEv;
        private eventList eventListDTChanged;
        private System.Windows.Forms.TabPage tabTariffShegEv;
        private eventList eventListShedEv;
        private System.Windows.Forms.TabPage tabCurrChangeEv;
        private eventList eventListCurrChanged;
        private System.Windows.Forms.TabPage tabEnLimEv;
        private eventList eventListEnLim;
        private System.Windows.Forms.TabPage tabPowLimEv;
        private eventList eventListPowLim;
        private eventList eventListCover;
        private System.Windows.Forms.TabPage tabRelay;
        private Relay relay1;
        private System.Windows.Forms.ToolStripButton toolbtnAdd;
        private System.Windows.Forms.ToolStripButton toolBtnDel;
        private System.Windows.Forms.TabPage tabSpecDays;
        private SpecDays specDays1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolBtnFromFile;
        private System.Windows.Forms.ToolStripButton toolBtnToFile;
        private System.Windows.Forms.TabPage tabCurrentEnergy;
        private CurrentEnergy currentEnergy1;
        private System.Windows.Forms.TabPage tabService;
        private ServicePanel servicePanel1;
        private About about1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripTextBox boxNetAddr;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.TabPage tabCommon;
        private CommonSettings commonSettings1;
    }
}

