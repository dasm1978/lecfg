﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class addSeasonDlg : Form {
        public addSeasonDlg() {
            InitializeComponent();
        }

        public Date getDate () {
            DateTime d = monthCalendar1.SelectionStart;
            return new Date(d.Day, d.Month);
        }
        public void setMinDate (Date minDate) {
            monthCalendar1.MaxSelectionCount = 1;
            monthCalendar1.MinDate = new DateTime(DateTime.Now.Year, minDate.m_month, minDate.m_day);
            monthCalendar1.MaxDate = new DateTime(DateTime.Now.Year, 12, 31);
            monthCalendar1.SetDate (new DateTime(DateTime.Now.Year, minDate.m_month, minDate.m_day));
        }
        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e) {

        }

        private void ok_Click(object sender, EventArgs e) {

        }
    }
}
