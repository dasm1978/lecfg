﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class pass : Form {
        public pass() {
            InitializeComponent();
        }
        public UInt32 getPass() {
            UInt32 pass = 0;
            bool res = false;
            res = UInt32.TryParse(boxPassword.Text, NumberStyles.Number, CultureInfo.CurrentCulture,  out pass);
            if (!res) {
                MessageBox.Show("Неверный формат пароля");
                return 0;
            }
            return pass;

        }

        private void btnOK_Click(object sender, EventArgs e) {

        }
    }
}
