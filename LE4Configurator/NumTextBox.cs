﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class NumTextBox : TextBox {
        public NumTextBox() {
            InitializeComponent();
            this.ShortcutsEnabled = false;
        }

        protected override void OnEnter(EventArgs e) {

        }
        //
        // Summary:
        //     Raises the System.Windows.Forms.Control.KeyPress event.
        //
        // Parameters:
        //   e:
        //     A System.Windows.Forms.KeyPressEventArgs that contains the event data.
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        protected override void OnKeyPress(KeyPressEventArgs e) {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) 
                   ) {
                e.Handled = true;
            }

            // only allow one decimal point
            if (((this as TextBox).Text.IndexOf('.') > -1)) {
                e.Handled = true;
            }
        }
    }
}
