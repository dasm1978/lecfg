﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class DisplaySettings : tabRDWRControl {

        public void initFromStruct (allDispSettings all) {
            var v = checkedListBox1.Items;
            foreach (var s in all.onOffList) {                
                checkedListBox1.SetItemChecked(v.IndexOf(s.name), s.isOn);
            }
            numericUpDown1.Value = all.time;
            checkedListBox1.SetItemChecked(v.IndexOf("Накопления суммарные"), true);
        }


        public void initbytesFromStr(string str) {
            string[] strs = str.Split('-');
            List<byte> lb = new List<byte>();
            foreach (string s in strs) {
                Int32 i = 0;
                Int32.TryParse(s, out i);
                lb.Add((byte)i);
            }
            initFromBytes(lb.ToArray());
        }


        private void initFromBytes(byte[] reply) {
            if ((byte)reply[0] == 0)
                numericUpDown1.Value = 1;
            else 
                numericUpDown1.Value = (byte)reply[0];
            int idx = 1;
            var v = checkedListBox1.Items;
            for (int i = 0; i < settings.Count; i++) {
                checkedListBox1.SetItemChecked(v.IndexOf(settings[i].name), reply[idx++] == 0 ? false : true);
            }
        }

        public static List<sDisplaySetting> settings = new List<sDisplaySetting>()  {
            { new sDisplaySetting("Накопления суммарные", true) },
            { new sDisplaySetting("Накопления по первому тарифу", true) },
            { new sDisplaySetting("Накопления по второму тарифу", true) },
            { new sDisplaySetting("Накопления по третьему тарифу", true) },
            { new sDisplaySetting("Накопления по четвертому тарифу", true) },
            { new sDisplaySetting("Напряжение", true) },
            { new sDisplaySetting("Ток", true) },
            { new sDisplaySetting("Мощность", true) },
            { new sDisplaySetting("Косинус", true) },
            { new sDisplaySetting("Частота", true) },
            { new sDisplaySetting("Дата", true) },
            { new sDisplaySetting("Время", true) },
            { new sDisplaySetting("Ошибки", true) },



            };
        public allDispSettings getAllSettings() {
            List<sDisplaySetting> l = new List<sDisplaySetting>();
            allDispSettings all = new allDispSettings();
            var v = checkedListBox1.Items;
            for (int i = 0; i < settings.Count; i++) {
                l.Add(new sDisplaySetting(settings[i].name, checkedListBox1.GetItemChecked(v.IndexOf(settings[i].name))));                
            }
            all.onOffList = l;
            all.time = (byte)numericUpDown1.Value;
            return all;
        }

        public override void OnRead(Protocol proto, Protocol.Commands cmdRead) {
            if (proto == null) {
                throw new ArgumentNullException(nameof(proto));
            }

            byte[] reply = proto.sendPacketGetReply(Protocol.Query.ENQ, (byte)cmdRead, Protocol.MAX_REPLYLEN);
            if (reply == null)
                return;
            initFromBytes(reply);
        }

        

        public override void OnWrite(Protocol proto, Protocol.Commands cmd) {
            //var v = checkedListBox1.CheckedItems;
            var v = checkedListBox1.Items;
            //bool b = 
            List<byte> pages = new List<byte>();
            //            
            pages.Add((byte)numericUpDown1.Value);
            for (int i = 0; i < settings.Count; i++) {
                pages.Add(checkedListBox1.GetItemChecked(v.IndexOf(settings[i].name)) ? (byte)0x01 : (byte)0x00);
            }
            byte[] reply = proto.sendPacketGetReply(Protocol.Query.REC, (byte)cmd, Protocol.MAX_REPLYLEN, pages.ToArray());
            if (reply != null)
                MessageBox.Show("Записано");
            else
                MessageBox.Show("Не записано");

        }

        public override bool OnSaveToFile() {
            return true;
        }


        public DisplaySettings() {
            InitializeComponent();
            numericUpDown1.Value = 1;
            var v = checkedListBox1.Items;
            checkedListBox1.SetItemChecked(v.IndexOf("Накопления суммарные"), true);
            //checkedListBox1.

            //checkedListBox1.Items.IndexOf("Накопления суммарные")
        }


        //public static CTariffIval constructFromStr(string timeStr, string tariffStr)
        //seasList[i].toXmlString()));

        public string asString() {
            string s = "";
            var v = checkedListBox1.Items;
            List<byte> pages = new List<byte>();

            pages.Add((byte)numericUpDown1.Value);
            for (int i = 0; i < settings.Count; i++) {              
                pages.Add(checkedListBox1.GetItemChecked(v.IndexOf(settings[i].name)) ? (byte)0x01 : (byte)0x00);
            }

            foreach (var b in pages) {
                s += b.ToString();
                s += "-";
            }
            return s;
        }
        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e) {

        }
    }

    public struct sDisplaySetting {
        public sDisplaySetting(string n, bool ison) {
            name = n; isOn = ison;
        }
        public string name;
        public bool isOn;
    }

    public struct allDispSettings {
        public List<sDisplaySetting> onOffList;
        public int time;
    }


}
