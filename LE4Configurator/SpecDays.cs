﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class SpecDays : tabRDWRControl {
        public SpecDays() {
            InitializeComponent();
        }

        private Form1 m_parentForm;


        public override bool OnSaveToFile() {
            return true;
        }

        public void setParent(Form1 par) {
            m_parentForm  = par;
        }

        public int getAvailableGraphs() {
            if (m_parentForm != null)
                return m_parentForm.getAvailableGraphs();
            else
                return 1;
        }
        public override void onAdd() {
            CSpecdayInfo i = new CSpecdayInfo();            
            onAdd(i);
        }

        public void ClearAll() {
            m_grids.Clear();            
            flowLayoutPanel1.Controls.Clear();
        }


        public void onAdd(CSpecdayInfo s) {
            List<Date> dis = new List<Date>();
            foreach (SpecDayGrid sg in m_grids) {
                dis.Add(sg.getDayInfo().m_date);
            }
            int n = m_parentForm.getAvailableGraphs();
            SpecDayGrid grid = new SpecDayGrid(s, "График ", flowLayoutPanel1, dis, n, m_parentForm);
            m_grids.Add(grid);
            grid.Width = flowLayoutPanel1.Width;
            grid.DrawGrid();
        }

        public List<CSpecdayInfo> getAllDaysInfo() {
            List<CSpecdayInfo> l = new List<CSpecdayInfo>();
            foreach (SpecDayGrid s in m_grids) {
                l.Add(s.getDayInfo());
            }
            return l;
        }


        public override void onDel() {
            if (m_grids.Count > 0) {
                SpecDayGrid last = m_grids.Last();
                flowLayoutPanel1.Controls.Remove(last);                
                m_grids.Remove(last);

            }
        }

        public List <CSpecdayInfo> getSpecDays () {
            List<CSpecdayInfo> l = new List<CSpecdayInfo>();          
            foreach (SpecDayGrid g in m_grids) {
                l.Add(g.getDayInfo());
            }
            return l;
        }

        private List<SpecDayGrid> m_grids = new List<SpecDayGrid>();
        public override void OnRead(Protocol proto, Protocol.Commands cmdRead) {
            m_parentForm.ReadShedsSeasonsDays();
        }

        public override void OnWrite(Protocol proto, Protocol.Commands cmd) {
            m_parentForm.WriteShedsSeasonsDays();
        }

    }
}
