﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace LE4Configurator {


    public class Date {
        public Date(int day, int month) {
            m_day = day;
            m_month = month;
        }

        public Date (DateTime d) {
            m_day = d.Day;
            m_month = d.Month ;
        }


        public static bool operator ==(DateTime dt, Date d) {
            if (dt.Day == d.m_day && dt.Month == d.m_month)
                return true;
            else return false;
        }
        public static bool operator !=(DateTime dt, Date d) {
            if (dt.Day != d.m_day || dt.Month != d.m_month)
                return true;
            else return false;
        }



        public string asString() {
            DateTime dt = new DateTime(2000, m_month, m_day);
            return dt.ToString("dd MMMM");
        }
        public int m_day;
        public int m_month;
    }

    public class CTariffIval {
        public CTariffIval() {

        }
        public CTariffIval(TimeSpan s, eTariff tar) {
            m_t = s;
            m_tar = tar;
        }
        public CTariffIval(DateTime s, eTariff tar) : this(new TimeSpan(s.Hour, s.Minute, s.Second), tar) {

        }

        

        public TimeSpan m_t = new TimeSpan(0, 0, 0);
        public eTariff m_tar = eTariff.TAR_1;
        public static string[] eTariffStr = { "Тариф 1", "Тариф 2", "Тариф 3", "Тариф 4" };

        public string tostringTime() {
            return String.Format("{0:D2}:{1:D2}", m_t.Hours, m_t.Minutes);
        }
        public string tostringTariff() {
            return eTariffStr[(int)m_tar];
        }

        public static CTariffIval constructFromStr(string timeStr, string tariffStr) {
            string[] hm = timeStr.Split(new char[2] { ':', '-' });
            CTariffIval ct = new CTariffIval();
            int hrs = 0;
            int min = 0;
            if (!Int32.TryParse(hm[0], out hrs))
                return null;
            if (!Int32.TryParse(hm[1], out min))
                return null;
            eTariff tar = eTariff.TAR_1;
            for (int i = 0; i < CTariffIval.eTariffStr.Count(); i++) {
                if (CTariffIval.eTariffStr[i] == tariffStr) {
                    tar = (eTariff)i;
                    break;
                }

            }
            return new CTariffIval(new TimeSpan(hrs, min, 0), tar);
        }

        static public Color getColor(eTariff t) {
            if (t == eTariff.TAR_1)
                return Color.AliceBlue;
            else if (t == eTariff.TAR_2)
                return Color.Azure;
            else if (t == eTariff.TAR_3)
                return Color.Bisque;
            else if (t == eTariff.TAR_4)
                return Color.Cyan;
            return Color.White;
        }


        public string getTartimeAsTxt() {
            return String.Format("Начало в {0,2:D2}:{1,2:D2}", m_t.Hours, m_t.Minutes);
        }
        public string getTarAsTxt() {
            if (m_tar == eTariff.TAR_1)
                return "Тариф 1";
            if (m_tar == eTariff.TAR_2)
                return "Тариф 2";
            if (m_tar == eTariff.TAR_3)
                return "Тариф 3";
            if (m_tar == eTariff.TAR_4)
                return "Тариф 4";
            return "Тариф 1";
        }

        public static int tarifComparer(CTariffIval lh, CTariffIval rh) {
            if (lh == null || rh == null)
                return 0;
            if (lh.m_t.TotalSeconds == rh.m_t.TotalSeconds)
                return 0;
            if (lh.m_t.TotalSeconds > rh.m_t.TotalSeconds)
                return 1;
            return -1;
        }
    }
}
