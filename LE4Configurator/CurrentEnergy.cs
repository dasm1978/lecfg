﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class CurrentEnergy : tabRDWRControl {
        public CurrentEnergy() {
            InitializeComponent();
        }
        public override void OnRead(Protocol proto, Protocol.Commands cmdRead) {                    
            dataGridView1.Rows.Clear();
            byte[] reply = proto.sendPacketGetReply(Protocol.Query.ENQ, (byte)cmdRead);
            if (reply != null) {

                dataGridView1.RowCount = 6;
                dataGridView1.ColumnCount = 2;

                dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                dataGridView1.Rows[0].Cells[0].Value = "Номер текущего тарифа";
                dataGridView1.Rows[0].Cells[1].Value = String.Format("{0}", reply[0] + 1);

                dataGridView1.Rows[1].Cells[0].Value = "Суммарная энергия по всем тарифам, кВт*ч";                
                dataGridView1.Rows[1].Cells[1].Value = String.Format("{0:0.00}", (double) BitConverter.ToInt32(reply, 1) / 1000);


                dataGridView1.Rows[2].Cells[0].Value = "Энергия по первому тарифу, кВт*ч";
                dataGridView1.Rows[2].Cells[1].Value = String.Format("{0:0.00}", (double)BitConverter.ToInt32(reply, 4 + 1) / 1000);

                dataGridView1.Rows[3].Cells[0].Value = "Энергия по второму тарифу, кВт*ч";
                dataGridView1.Rows[3].Cells[1].Value = String.Format("{0:0.00}", (double)BitConverter.ToInt32(reply, 4 * 2 + 1) / 1000);

                dataGridView1.Rows[4].Cells[0].Value = "Энергия по третьему тарифу, кВт*ч";
                dataGridView1.Rows[4].Cells[1].Value = String.Format("{0:0.00}", (double)BitConverter.ToInt32(reply, 4 * 3 + 1) / 1000);

                dataGridView1.Rows[5].Cells[0].Value = "Энергия по четвертому тарифу, кВт*ч";
                dataGridView1.Rows[5].Cells[1].Value = String.Format("{0:0.00}", (double)BitConverter.ToInt32(reply, 4 * 4 + 1) / 1000);

            }

        }

    }
}
