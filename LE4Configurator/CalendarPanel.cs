﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class CalendarPanel : tabRDWRControl {
        private Form1 m_parentForm;
        public void setParentForm(Form1 form) {
            m_parentForm = form;
        }
        private int maxSeasons = 12;
        private Date m_startDate = new Date(1, 1);
        private Color[] daysColors = { Color.LightBlue, Color.LightYellow, Color.LightPink, Color.LightCoral };
        List<SeasonGrid> m_listGrids = new List<SeasonGrid>();

        private SeasonGrid makeGrid(CSeason s, CalendarPanel parent) {
            SeasonGrid grid = new SeasonGrid("Сезон с" + Environment.NewLine + s.start.asString(), parent, flowLayoutPanel1, s);
            return grid;
        }





//        //seasons string
//        List<byte> arrSeas = new List<byte>();
//                for (byte i = 0; i<seasons.Count; i++) { // 
//                    CSeason s = seasons[i];
//        arrSeas.AddRange(new byte[2] { (byte) s.start.m_day, (byte)(s.start.m_month - 1) }); // сезоны в графиках месяца нумеруют от 1, а в протоколе от 0
//                }
//arrSeas.AddRange(new byte[2] { (byte)31, 11 });
//                byte[] arg2 = new byte[256];
//Array.Copy(arrSeas.ToArray(), 0, arg2, 0, arrSeas.ToArray().Count());
//                Array.Resize(ref arg2, arrSeas.ToArray().Count());
//                arg2 = BCD.bin2bcd(arg2);
//                byte[] reply2 = proto.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.WR_TARIFF_SEASONS, Protocol.MAX_REPLYLEN, arg2, 3000);
//                if (reply2 == null)
//                    throw new Exception("no reply season");


public override bool OnSaveToFile() {
            return true;
        }

        public int getAvailableGraphs() {
            if (m_parentForm != null)
                return m_parentForm.getAvailableGraphs();
            else
                return 1;
        }
        public CalendarPanel() {
            InitializeComponent();
            m_listGrids.Add(makeGrid(new CSeason(), this));
        }

        public void ClearAll () {
            m_listGrids.Clear();
            flowLayoutPanel1.Controls.Clear();
        }
        public  void onAdd(CSeason s) {
            SeasonGrid g = makeGrid(s, this);
            m_listGrids.Add(g);
            g.DrawGrid();
        }
        public override void onAdd() {
            addSeasonDlg d = new addSeasonDlg();
            d.setMinDate(m_listGrids.Last().getDate());
            d.StartPosition = FormStartPosition.Manual;
            Point p = Cursor.Position;
            p.Offset(-20, 20);
            d.Location = p;
            DialogResult res = d.ShowDialog(this);
            if (res != DialogResult.OK)
                return;
            CSeason s = new CSeason();
            s.start = d.getDate();
            onAdd(s);
        }

        public List <CSeason>  getAllSeasons() {
            List<CSeason> list = new List<CSeason>();
            foreach (SeasonGrid s in m_listGrids) {
                CSeason seas = new CSeason();
                seas.start = s.getDate();
                seas.index = s.getGraphsIdxs(); //TODO OMG
                list.Add(seas);
            }
            return list;
        }

        public bool isUsed(int n) {
            foreach (SeasonGrid s in m_listGrids) {
                if (s.isUsed(n))
                    return true;
            }
            return false;
        }
        public override void onDel() {
            if (m_listGrids.Count <= 1)
                return;            
            flowLayoutPanel1.Controls.Remove(m_listGrids.Last());
            m_listGrids.Remove(m_listGrids.Last());
        }

        private void flowLayoutPanel1_ClientSizeChanged(object sender, EventArgs e) {
            foreach (SeasonGrid s in m_listGrids) {
                s.Width = this.Width;
                s.DrawGrid();
            }
        }

        public override void OnRead(Protocol proto, Protocol.Commands cmd) {
            m_parentForm.ReadShedsSeasonsDays();
        }

        public override void OnWrite(Protocol proto, Protocol.Commands cmd) {
            m_parentForm.WriteShedsSeasonsDays();
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e) {

        }
    }
}
