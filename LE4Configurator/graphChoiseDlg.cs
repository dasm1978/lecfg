﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class GraphChoiseDlg : Form {
        public GraphChoiseDlg(int availableGraphs) {
            InitializeComponent();
            for (int i = 0; i < availableGraphs; i++) {
                comboChoise.Items.Add(i.ToString());
            }            
        }

        public void setSelected(int n) {
            comboChoise.SelectedIndex = n;
        }
        public int getGraph() {
            return comboChoise.SelectedIndex;
        }

        private void ok_Click(object sender, EventArgs e) {

        }
    }
}
