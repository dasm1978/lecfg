﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using System.Threading;

namespace LE4Configurator {
    class BackThread {
        private Thread m_thread = null;
        private Protocol m_proto = null;
        private sRealMeasurements m_meas = null;
        private bool run = false;
        public void setProto(Protocol proto) {
            m_proto = proto;
        }
        public BackThread() {

        }
        public sRealMeasurements getMeas() {            
            return m_meas;
        }

        public void start() {
            run = true;

            m_thread = new Thread(this.threadFoo);
            m_thread.Start();
        }
        public void stop() {
            run = false;
            if (m_thread != null)
                m_thread.Join();
        }
        private void threadFoo(object obj) {
            while (run) {
                if (m_proto.isOpen()) {
                    
                    byte[] rawMeas = m_proto.sendPacketGetReply(Protocol.Query.ENQ, Protocol.tocmd(Protocol.Commands.RD_MEASURE), sRealMeasurements.rawLen);
                    if ((rawMeas != null) && (rawMeas.Length == sRealMeasurements.rawLen)) {
                        sRealMeasurements s = new sRealMeasurements();
                        s.asRaw = rawMeas;
                        m_meas = s;
                    }

                }
                Thread.Sleep(100);
            }

        }
    }
}
