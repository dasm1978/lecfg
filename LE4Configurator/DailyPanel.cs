﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;


namespace LE4Configurator {



    public partial class DailyPanel : tabRDWRControl {

        public DailyPanel() {
            InitializeComponent();
            onAdd();
        }

        public override bool OnSaveToFile() {
            return true;
        }


        public List<List<CTariffIval>> getAllTariifs() {
            List<List<CTariffIval>> allsheds = new List<List<CTariffIval>>(); // all lists
            foreach (CTimeGrid g in m_timegrids) {
                allsheds.Add(g.getList());
            }
            return allsheds;
        }

        public void setAllTariffs(List<List<CTariffIval>> all) {
            ClearAll();
            foreach (List<CTariffIval> l in all) {
                onAdd(l);
            }
        }

        public void ClearAll() {
            foreach (CTimeGrid c in m_timegrids) {
                flowLayoutPanel1.Controls.Remove(c);
            }
            m_timegrids.Clear();
        }

        private Form1 m_parentForm;
        public void setParentForm(Form1 form) {
            m_parentForm = form;
        }

        int N = 0;

        private List<CTimeGrid> m_timegrids = new List<CTimeGrid>();

        public int Count() {
            return m_timegrids.Count();
        }


        public void onLoadFromFile() {
            FileDialog dlg = new OpenFileDialog();
            dlg.CheckFileExists = true;
            dlg.Filter = "Data Files (*.xml)|*.xml";
            dlg.DefaultExt = "xml";
            dlg.AddExtension = true;
            if (dlg.ShowDialog(this) != DialogResult.OK)
                return;
            foreach (CTimeGrid c in m_timegrids) {
                flowLayoutPanel1.Controls.Remove(c);
            }
            m_timegrids.Clear();

            List<List<CTariffIval>> list = new List<List<CTariffIval>>();
            XDocument x = XDocument.Load(dlg.FileName);
            XElement root = x.Root;
            IEnumerable<XElement> el =
                from nd in root.Elements()
                where nd.Name.LocalName == "graph"
                select nd;
            foreach (XElement elem in el) { // here graps
                IEnumerable<XElement> elpoint =
                    from nd in elem.Elements()
                    where nd.Name.LocalName == "point"
                    select nd;

                List<CTariffIval> listOne = new List<CTariffIval>();
                foreach (XElement xe1 in elpoint) {
                    listOne.Add(CTariffIval.constructFromStr(xe1.Attribute("time").Value, xe1.Attribute("tariff").Value));
                }
                onAdd(listOne);
            }
        }


        public void onAdd(List<CTariffIval> l = null) {
            if (m_timegrids.Count == 48)
                return;
            CTimeGrid grid = new CTimeGrid("График " + m_timegrids.Count.ToString(), flowLayoutPanel1);
            grid.Width = flowLayoutPanel1.Width;
            List<CTariffIval> list = new List<CTariffIval>();
            CTariffIval first = new CTariffIval(new TimeSpan(0, 0, 0), eTariff.TAR_1);
            list.Add(first);
            m_timegrids.Add(grid);
            if (l == null)
                grid.setData(list);
            else
                grid.setData(l);
        }

        public override void onAdd() {
            onAdd(null);
        }


        public override void onDel() {
            if (m_timegrids.Count == 0) return;
            CTimeGrid last = m_timegrids.Last();

            if (m_parentForm.canDelete(m_timegrids.Count - 1)) {
                flowLayoutPanel1.Controls.Remove(last);
                m_timegrids.Remove(last);
            }
            else {
                DlgCantDelete d = new DlgCantDelete();
                d.StartPosition = FormStartPosition.Manual;
                d.Location = PointToScreen(PointToClient(Cursor.Position));
                DialogResult res = d.ShowDialog(this);
            }
        }


        public static List<CTariffIval> makeCTarifListFrombytes(byte[] b) {
            List<CTariffIval> l = new List<CTariffIval>();
            for (int i = 0; i < b.Count(); i += 2) {
                CTariffIval val = new CTariffIval();
                byte[] ab = new byte[2];
                Array.Copy(b, i, ab, 0, 2); // берем два байта мин час и номер тарифа
                byte mins = BCD.bcd2bin((byte)ab[0]);
                byte hrs = BCD.bcd2bin((byte)(ab[1] & 0x3f));
                byte tarNo = ((byte)(ab[1] >> 6));
                val.m_tar = (eTariff)tarNo;
                val.m_t = new TimeSpan(hrs, mins, 0);
                l.Add(val);
            }
            return l;
        }

        public static byte[] makeByteArrayFromList_CTariffIval(List<CTariffIval> list) {
            List<byte> arr = new List<byte>();
            foreach (CTariffIval c in list) {
                arr.AddRange(new byte[2] { BCD.bin2bcd((byte)c.m_t.Minutes), (byte)(BCD.bin2bcd((byte)c.m_t.Hours) + (byte)((byte)c.m_tar << 6)) });
            }
            arr.AddRange(new byte[2] { BCD.bin2bcd((byte)0), (byte)(BCD.bin2bcd((byte)24) + (byte)((byte)0 << 6)) }); // last one always 24
            return arr.ToArray();
        }

        public override void OnRead(Protocol proto, Protocol.Commands cmd) {
            m_parentForm.ReadShedsSeasonsDays();
        }

        public override void OnWrite(Protocol proto, Protocol.Commands cmd) {
            m_parentForm.WriteShedsSeasonsDays();
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e) {

        }

        private void flowLayoutPanel1_ClientSizeChanged(object sender, EventArgs e) {
            foreach (CTimeGrid c in m_timegrids) {
                c.Width = flowLayoutPanel1.Width;
                c.DrawGrid();
            }
        }
    }
}
