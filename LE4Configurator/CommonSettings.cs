﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class CommonSettings : tabRDWRControl {
        public CommonSettings() {
            InitializeComponent();
        }

        private void btnWrPass_Click(object sender, EventArgs e) {
            if (boxOldPass.Text != boxNewPass.Text) {
                MessageBox.Show("Пароли не совпадают!");
                return;
            }
            string newPass = boxNewPass.Text;
            UInt32 passDec = 0;
            bool res = UInt32.TryParse(newPass, out passDec);
            if (!res) {
                MessageBox.Show("Пароль неправильного формата!");
                return;
            }

            byte[] reply = m_proto.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.WR_PASSWORD, Protocol.MAX_REPLYLEN, BitConverter.GetBytes(passDec));
            if (reply != null) {
                if (reply.Count() > 0) {
                    MessageBox.Show("Записан!");
                    return;
                }
            }
            MessageBox.Show("Нет ответа");



        }

        public void setProto(Protocol proto) {
            m_proto = proto;
        }
        private Protocol m_proto = null;

        public class CCounterSettings {
            public byte[] asRaw {
                set {
                    gmt = (UInt32)((UInt32)value[0] | (UInt32)(value[1] << 8) | (UInt32)(value[2] << 16) | (UInt32)(value[3] << 24));
                    serial = (UInt32)((UInt32)value[4] | (UInt32)(value[5] << 8) | (UInt32)(value[6] << 16) | (UInt32)(value[7] << 24));
                    netAddr = (UInt32)((UInt32)value[8] | (UInt32)(value[9] << 8) | (UInt32)(value[10] << 16) | (UInt32)(value[11] << 24));
                    m_type = value[12];
                    hwver = value[13];
                    swver = value[14];
                }
                get {
                    byte[] raw = new byte[rawLen];
                    Array.Copy(BitConverter.GetBytes(gmt), 0, raw, 0, 4);
                    Array.Copy(BitConverter.GetBytes(serial), 0, raw, 4, 4);
                    Array.Copy(BitConverter.GetBytes(netAddr), 0, raw, 8, 4);
                    raw[12] = m_type;
                    raw[13] = hwver;
                    raw[14] = swver;
                    return raw;
                }

            }

            public int rawLen { get { return 4 + 4 + 4 + 3; } }

            public UInt32 gmt;
            public UInt32 serial;
            public UInt32 netAddr;
            public byte m_type;
            public byte hwver;
            public byte swver;

        }

        private void btnWrNetAddr_Click(object sender, EventArgs e) {
            try {
                UInt32 netAddr = 0;
                if (boxNetAddr.Text.Length > 8)
                    throw new Exception("Неверный адрес");

                bool res = UInt32.TryParse(boxNetAddr.Text, out netAddr);
                if (!res)
                    throw new Exception("Неверный адрес");

                byte[] reply = m_proto.sendPacketGetReply(Protocol.Query.ENQ, (byte)Protocol.Commands.RD_COUNTER_INFO, Protocol.MAX_REPLYLEN);
                if (reply == null)
                    throw new Exception("Не отвечает");

                CCounterSettings set = new CCounterSettings();
                set.asRaw = reply;
                set.netAddr = netAddr;
                byte[] reply2 = m_proto.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.WR_COUNTER_INFO, Protocol.MAX_REPLYLEN, set.asRaw);
                if (reply2 == null)
                    throw new Exception("Не отвечает");
                if (reply2.Count() == 0)
                    throw new Exception("Не отвечает");
                MessageBox.Show("Записан!");
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnClrProfile_Click(object sender, EventArgs e) {
            byte[] reply = m_proto.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.EX_CLR_POW_PROFILES, Protocol.MAX_REPLYLEN);
            if (reply != null) {
                MessageBox.Show("Очищен");
                return;
            }
            MessageBox.Show("Нет ответа");

        }

        private void btnClrErr_Click(object sender, EventArgs e) {
            byte[] reply = m_proto.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.EX_CLR_ERRS, Protocol.MAX_REPLYLEN);
            if (reply != null) {
                MessageBox.Show("Очищены");
                return;
            }
            MessageBox.Show("Нет ответа");
        }

        private void btnWrProfIval_Click(object sender, EventArgs e) {
            int recT = (int)upDwnRecIval.Value * 60;
            byte[] reply = m_proto.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.WR_PROFILE_TIME, Protocol.MAX_REPLYLEN, BitConverter.GetBytes(recT));
            if (reply != null)
                MessageBox.Show("Записано");
            else
                MessageBox.Show("Не записано");
        }
    }
}
