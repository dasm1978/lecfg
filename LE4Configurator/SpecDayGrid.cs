﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class SpecDayGrid : UserControl {
        public SpecDayGrid() {
            InitializeComponent();
        }
        private List<Date> m_disabled;
        int m_availableGraphs = 0;
        private Form1 m_form;
        public SpecDayGrid(CSpecdayInfo info,  string name, Control parent, List<Date> disabled, int availableGraphs, Form1 pForm) {
            m_info = info;
            m_form = pForm;
            m_disabled = disabled;
            m_availableGraphs = availableGraphs;
            this.Parent = parent;
            InitializeComponent();
            this.Height = 35;
            Width = parent.Width - 5; // TODO ????

            m_Name = name;
            grid.RowCount = 1;
            grid.Rows[0].Height = this.Height;

            grid.AllowUserToAddRows = false;
            grid.AllowUserToResizeColumns = false;
            grid.AllowUserToOrderColumns = false;

            grid.AllowUserToResizeRows = false;
            grid.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            grid.ColumnHeadersVisible = false;
            grid.Enabled = false;
            DrawGrid();
        }

        public void DrawGrid() {
            grid.ColumnCount = 1;
            grid.RowCount = 1;
            grid.Rows[0].Cells[0].Value = m_info.m_date.asString();
            //grid.RowHeadersWidth = 120;
            grid.RowHeadersVisible = false;
            grid.Rows[0].HeaderCell.Value = m_Name;

            DataGridViewCellStyle st = new DataGridViewCellStyle();
            //st.BackColor = CTariffIval.getColor(m_tariffs[i].m_tar);
            st.WrapMode = DataGridViewTriState.True;
            st.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grid.Rows[0].Cells[0].Style = st;

            Utilities.MakeEq(grid);
        }

        private void редактироватьToolStripMenuItem_Click(object sender, EventArgs e) {
            DlgEditSpecDay d = new DlgEditSpecDay(m_info,  m_disabled, m_form.getAvailableGraphs());
            

            d.StartPosition = FormStartPosition.Manual;
            Point m_clickPoint = contextMenuStrip1.PointToScreen(new Point(0, 0));
            d.Location = PointToScreen(PointToClient(Cursor.Position));
            DialogResult res = d.ShowDialog(this);
            if (res == DialogResult.OK) {
                m_info = d.getDayInfo();
                DrawGrid();
            }
        }

        public CSpecdayInfo getDayInfo() {
            return m_info;
        }

        private string m_Name;
        private CSpecdayInfo m_info = new CSpecdayInfo();

    }


    public class CSpecdayInfo {

        public DateTime getDateTime() {
            DateTime dt = new DateTime(2000, m_date.m_month, m_date.m_day);
            return dt;
        }

        public static CSpecdayInfo constructFromStr(string str) {
            CSpecdayInfo s = new CSpecdayInfo();
            string[] st = str.Split('-');
            Int32.TryParse(st[0], out s.m_date.m_day);
            Int32.TryParse(st[1], out s.m_date.m_month);          
            return s;
        }
        public string toXmlString() {
            return String.Format("{0}-{1}", m_date.m_day, m_date.m_month);
        }



        public Date m_date = new Date(1, 1);
        //public int nGraph = 0;
    }
}
