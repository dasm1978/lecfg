﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LE4Configurator {

    public class CTariffIval {
        public CTariffIval() {

        }
        public CTariffIval(TimeSpan s, eTariff tar) {
            m_t = s;
            m_tar = tar;
        }
        public CTariffIval(DateTime s, eTariff tar) : this(new TimeSpan(s.Hour, s.Minute, s.Second), tar) {

        }

        public TimeSpan m_t = new TimeSpan(0, 0, 0);
        public eTariff m_tar = eTariff.TAR_1;
        static public Color getColor(eTariff t) {
            if (t == eTariff.TAR_1)
                return Color.AliceBlue;
            else if (t == eTariff.TAR_2)
                return Color.Azure;
            else if (t == eTariff.TAR_3)
                return Color.Bisque;
            else if (t == eTariff.TAR_4)
                return Color.Cyan;
            return Color.White;
        }

        public string getTartimeAsTxt() {
            return String.Format("Начало в {0,2:D2}:{1,2:D2}", m_t.Hours, m_t.Minutes);
        }
        public string getTarAsTxt() {
            if (m_tar == eTariff.TAR_1)
                return "Тариф 1";
            if (m_tar == eTariff.TAR_2)
                return "Тариф 2";
            if (m_tar == eTariff.TAR_3)
                return "Тариф 3";
            if (m_tar == eTariff.TAR_4)
                return "Тариф 4";
            return "Тариф 1";
        }

        public static int tarifComparer(CTariffIval lh, CTariffIval rh) {
            if (lh == null || rh == null)
                return 0;
            if (lh.m_t.TotalSeconds == rh.m_t.TotalSeconds)
                return 0;
            if (lh.m_t.TotalSeconds > rh.m_t.TotalSeconds)
                return 1;
            return -1;
        }
    }
}
