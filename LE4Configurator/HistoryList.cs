﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class HistoryList : tabRDWRControl {


        private string formatDayDate(byte []reply) {        
            return   String.Format("{0:00}-{1:00}-{2:00}", BCD.bcd2bin(reply[0]), BCD.bcd2bin(reply[1] + 1 /*month from 0*/), BCD.bcd2bin(reply[2]));        
        }

        private string formatMonthDate(byte[] reply) {
            return String.Format("{0:00}-{1:00}", BCD.bcd2bin(reply[0] + 1 /*month from 0*/), BCD.bcd2bin(reply[1]));
        }

        private string formatYearDate(byte[] reply) {
            return String.Format("{0:0000}", 2000 + BCD.bcd2bin(reply[0]  /*year from 0*/));
        }

        private string [] formatReply (byte []reply) {
            string []ret = new string[5];
            for (int p = 0; p < 5; p++) {
                ret[p] = String.Format("{0:0.00}", (double) BitConverter.ToUInt32(reply, 4 + p * 4) / 1000);                                
            }
            return ret;
        }


        public HistoryList() {
            InitializeComponent();
            dataGridView1.ColumnCount = 1 + 1 + 4;
            dataGridView1.Columns[0].HeaderCell.Value = "Дата";
            dataGridView1.Columns[1].HeaderCell.Value = "Сумм. энергия, кВт*ч";
            dataGridView1.Columns[2].HeaderCell.Value = "Энергия тар 1, кВт*ч";
            dataGridView1.Columns[3].HeaderCell.Value = "Энергия тар 2, кВт*ч";
            dataGridView1.Columns[4].HeaderCell.Value = "Энергия тар 3, кВт*ч";
            dataGridView1.Columns[5].HeaderCell.Value = "Энергия тар 4, кВт*ч";
        }

        public override void OnRead(Protocol proto, Protocol.Commands cmdRead) {            
            dataGridView1.Rows.Clear();
            for (int i = 0; i < m_depth ; i++) {
                byte[] arg = new byte[1] { (byte) i };

                byte[] reply = proto.sendPacketGetReply(Protocol.Query.ENQ, (byte)cmdRead, Protocol.MAX_REPLYLEN, arg, 3000);
                if (reply == null || reply.Length < 20)
                    break;                               

                
                dataGridView1.Rows.Add();       

                if (m_type == "day")
                    dataGridView1.Rows[i].Cells[0].Value = formatDayDate(reply);
                else if (m_type == "month") {
                    dataGridView1.Rows[i].Cells[0].Value = formatMonthDate(reply);
                }
                else if (m_type == "year") {
                    dataGridView1.Rows[i].Cells[0].Value = formatYearDate(reply);
                }
                string[] powers = formatReply(reply);
                for (int p = 0; p < 5; p++) {
                    dataGridView1.Rows[i].Cells[p + 1].Value = powers[p];
                }
            }
        }

        public void setParam (Protocol proto, int histDepth, Protocol.Commands readCmd, string type) {          
            m_depth = histDepth;
            m_type = type;
        }
        
        
        private int m_depth;
        private string m_type;
    }
}
