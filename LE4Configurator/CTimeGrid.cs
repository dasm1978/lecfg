﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class CTimeGrid : UserControl {
        private int maxTar = 11;
        private List<CTariffIval> m_tariffs = new List<CTariffIval>();
        private int m_trackCell = -1;  // отслеживаем где выпадет меню контесктное
        public string m_Name { get; set; }
        public CTimeGrid(string name, Control parent) {
            this.Parent = parent;
            InitializeComponent();
            this.Height = 35;
            Width = parent.Width - 5; // TODO ????

            m_Name = name;
            grid.RowCount = 1;
            grid.Rows[0].Height = this.Height;

            grid.AllowUserToAddRows = false;
            grid.AllowUserToResizeColumns = false;
            grid.AllowUserToOrderColumns = false;

            grid.AllowUserToResizeRows = false;
            grid.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            grid.ColumnHeadersVisible = false;
            setData(m_tariffs);
        }

        public List <CTariffIval> getList() {
            return m_tariffs;
        }
        public void setData(List<CTariffIval> tarifs) {
            m_tariffs = tarifs;
            DrawGrid();
        }

        public void DrawGrid() {
            if (m_tariffs.Count == 0) {
                grid.ColumnCount = 1;
                grid.Rows[0].Cells[0].Value = "Тариф 1 круглые сутки";
            }
            else {
                grid.RowTemplate.Height = 3;
                grid.RowTemplate.MinimumHeight = 3;
                grid.ColumnCount = m_tariffs.Count;
                for (int i = 0; i < m_tariffs.Count; i++) {
                    string txt = m_tariffs[i].getTarAsTxt() + Environment.NewLine + m_tariffs[i].getTartimeAsTxt();
                    grid.Rows[0].Cells[i].Value = txt;
                    DataGridViewCellStyle st = new DataGridViewCellStyle();
                    st.BackColor = CTariffIval.getColor(m_tariffs[i].m_tar);
                    st.WrapMode = DataGridViewTriState.True;
                    st.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    grid.Rows[0].Cells[i].Style = st;

                }
            }
            grid.RowHeadersWidth = 120;
            grid.Rows[0].HeaderCell.Value = m_Name;
            Utilities.MakeEq(grid);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) {

        }


        

        
        private void grid_CellMouseEnter(object sender, DataGridViewCellEventArgs e) {
            m_trackCell = e.ColumnIndex;            
        }


        private void editToolStripMenuItem_Click(object sender, EventArgs e) {
            int iTar = m_trackCell;
            List<eTariff> l = new List<eTariff>();
            eTariff[] ee = { eTariff.TAR_1, eTariff.TAR_2, eTariff.TAR_3, eTariff.TAR_4 };
            l.AddRange(new List<eTariff>(ee));
            if (iTar < m_tariffs.Count - 1) {
                l.Remove(m_tariffs[iTar + 1].m_tar);// not succ
            }
            if (iTar > 0) {
                l.Remove(m_tariffs[iTar - 1].m_tar);// not prec
            }
            AddEditDlg d = new AddEditDlg(m_tariffs[iTar], l);

            if (iTar == 0) {
                d.blockTimeChoose(); // нельзя в первой точке менять время
            }
            else { // в остальных можно, но не менее {
                d.setMinTime(m_tariffs[iTar - 1].m_t + new TimeSpan(0, 1, 0));  // но можно установить на 1 минуту минимум больше
                if (iTar < m_tariffs.Count - 1) {
                    d.setMaxTime(m_tariffs[iTar + 1].m_t - new TimeSpan(0, 1, 0));
                }

            }
            d.StartPosition = FormStartPosition.Manual;
            Point m_clickPoint = contextMenuStrip1.PointToScreen(new Point(0, 0));
            d.Location = PointToScreen(PointToClient(Cursor.Position));
            DialogResult res = d.ShowDialog(this);
            if (res == DialogResult.OK) {
                m_tariffs[iTar] = d.getResult();
                m_tariffs.Sort(CTariffIval.tarifComparer);
                DrawGrid();
            }
        }

        private void menuAdd_Click(object sender, EventArgs e) {
            int iTar = m_trackCell;

            CTariffIval start = new CTariffIval(m_tariffs[iTar].m_t + new TimeSpan(1, 0, 0), m_tariffs[iTar].m_tar); // сразу предложим + час            

            List<eTariff> l = new List<eTariff>();
            eTariff[] ee = { eTariff.TAR_1, eTariff.TAR_2, eTariff.TAR_3, eTariff.TAR_4 };
            l.AddRange(new List<eTariff>(ee));
            l.Remove(m_tariffs[iTar].m_tar);// not current
            if (iTar < m_tariffs.Count - 1) {
                l.Remove(m_tariffs[iTar + 1].m_tar);// not succ
            }
            AddEditDlg d = new AddEditDlg(start, l);
            d.setMinTime(m_tariffs[iTar].m_t + new TimeSpan(0, 1, 0));  // но можно установить на 1 минуту минимум больше
            if (iTar < m_tariffs.Count - 1) {
                d.setMaxTime(m_tariffs[iTar + 1].m_t + new TimeSpan(0, 1, 0));
            }
            d.StartPosition = FormStartPosition.Manual;

            Point m_clickPoint = contextMenuStrip1.PointToScreen(new Point(0, 0));
            d.Location = PointToScreen(PointToClient(Cursor.Position));
            DialogResult res = d.ShowDialog(this);
            if (res == DialogResult.OK) {
                m_tariffs.Insert(iTar, d.getResult());
                m_tariffs.Sort(CTariffIval.tarifComparer);
                CheckAndRemoveDups();
                DrawGrid();
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e) {
            int iTar = m_trackCell;
            if (iTar < 0) {
                e.Cancel = true;
            }

            int ia = contextMenuStrip1.Items.IndexOf(menuAdd);
            int id = contextMenuStrip1.Items.IndexOf(menuDel);
            if (m_tariffs.Count == maxTar) {
                contextMenuStrip1.Items[ia].Enabled = false;
            }
            else
                contextMenuStrip1.Items[ia].Enabled = true;
            if (iTar == 0) {
                contextMenuStrip1.Items[id].Enabled = false;
            }
            else
                contextMenuStrip1.Items[id].Enabled = true;
        }

        private void CheckAndRemoveDups() {
            if (m_tariffs.Count < 2)
                return;
            for (int i = 0; i < m_tariffs.Count - 1; i++) {
                if (m_tariffs[i].m_tar == m_tariffs[i + 1].m_tar)
                    m_tariffs.RemoveAt(i + 1);
            }
        }
        private void menuDel_Click(object sender, EventArgs e) {
            int iTar = m_trackCell;
            if (iTar != 0) {
                m_tariffs.RemoveAt(iTar);
                CheckAndRemoveDups();
                DrawGrid();
            }
        }

        
    }

    public enum eTariff { TAR_1, TAR_2, TAR_3, TAR_4 };

    public class TarifString {
        public static eTariff fromStr(string s) {
            if (s == "Тариф 1")
                return eTariff.TAR_1;
            if (s == "Тариф 2")
                return eTariff.TAR_2;
            if (s == "Тариф 3")
                return eTariff.TAR_3;
            if (s == "Тариф 4")
                return eTariff.TAR_4;
            return eTariff.TAR_1;
        }
        public static string fromEtarif(eTariff e) {
            if (e == eTariff.TAR_1)
                return "Тариф 1";
            if (e == eTariff.TAR_2)
                return "Тариф 2";
            if (e == eTariff.TAR_3)
                return "Тариф 3";
            if (e == eTariff.TAR_4)
                return "Тариф 4";
            return "Тариф 1";
        }
    }
    
}
