﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Xml;
using System.Xml.Linq;
using System.Threading;
using System.Runtime.InteropServices;


namespace LE4Configurator {
    public partial class Form1 : Form {

        private UInt32 password = 0x1b207;
        //private enum MEAS_ROW { U, I, P, Q, F, Ap, COS };
        private enum MEAS_ROW { U, I, P, F, Ap, COS };
        private Protocol m_protocol = new Protocol();
        private BackThread m_thread = new BackThread();
        private SerialPort m_serial = new SerialPort();

        private DataGridView viewCoverEvents = new DataGridView();
        private DataGridView viewPowerOnEvents = new DataGridView();
        private DataGridView viewMeasurements = new DataGridView();
        public Form1() {           

            InitializeComponent();
            calendarPanel1.setParentForm(this);
            dailyPanel1.setParentForm(this);
            comboUserAdmin.SelectedIndex = 0;
            // remove tabs from main tab conrol
            tabAll.Appearance = TabAppearance.FlatButtons;
            tabAll.ItemSize = new Size(0, 1);
            tabAll.SizeMode = TabSizeMode.Fixed;
            splitContainer1.SplitterDistance = 260;

            m_thread.setProto(m_protocol);


            //TabPage pageEv = new TabPage("cover");
            //pageEv.Name = "cover";
            //tabAll.TabPages.Add(pageEv);

            //viewCoverEvents.RowTemplate.Height = 20;
            //viewCoverEvents.ColumnCount = 3;
            //viewCoverEvents.RowHeadersVisible = false;
            //viewCoverEvents.Columns[0].HeaderText = "Number";
            //viewCoverEvents.Columns[1].HeaderText = "Date Time";
            //viewCoverEvents.Columns[2].HeaderText = "Events";
            //viewCoverEvents.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //viewCoverEvents.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //viewCoverEvents.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            //viewCoverEvents.Parent = pageEv;
            //viewCoverEvents.Dock = DockStyle.Fill;


            //TabPage pagePowerEv = new TabPage("power");
            //pagePowerEv.Name = "power";
            //tabAll.TabPages.Add(pagePowerEv);

            //viewPowerOnEvents.RowTemplate.Height = 20;
            //viewPowerOnEvents.ColumnCount = 3;
            //viewPowerOnEvents.RowHeadersVisible = false;
            //viewPowerOnEvents.Columns[0].HeaderText = "Number";
            //viewPowerOnEvents.Columns[1].HeaderText = "Date Time";
            //viewPowerOnEvents.Columns[2].HeaderText = "Events";
            //viewPowerOnEvents.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //viewPowerOnEvents.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //viewPowerOnEvents.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            //viewPowerOnEvents.Parent = pagePowerEv;
            //viewPowerOnEvents.Dock = DockStyle.Fill;


            TabPage pageMeasurements = new TabPage("measurements");
            pageMeasurements.Name = "measurements";
            tabAll.TabPages.Add(pageMeasurements);
            viewMeasurements.RowTemplate.Height = 20;
            viewMeasurements.ColumnCount = 2;
            viewMeasurements.RowHeadersVisible = false;
            viewMeasurements.Columns[0].HeaderText = "Параметр";
            viewMeasurements.Columns[1].HeaderText = "Значение";
            viewMeasurements.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            viewMeasurements.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            viewMeasurements.Parent = pageMeasurements;
            viewMeasurements.Dock = DockStyle.Fill;
            viewMeasurements.Enabled = false;
            viewMeasurements.Rows.Add(6);
            int i = 0;
            viewMeasurements.Rows[i++].Cells[0].Value = "Напряжение, В";
            viewMeasurements.Rows[i++].Cells[0].Value = "Ток, А";
            viewMeasurements.Rows[i++].Cells[0].Value = "Активная мощность, кВт";
            //viewMeasurements.Rows[i++].Cells[0].Value = "Реактивная мощность, кВт";
            viewMeasurements.Rows[i++].Cells[0].Value = "Частота, Гц";
            viewMeasurements.Rows[i++].Cells[0].Value = "Активная энергия, кВт*ч";
            viewMeasurements.Rows[i++].Cells[0].Value = "Коэффициент мощности ";


            settingsPanel1.setProto(m_protocol);
            commonSettings1.setProto(m_protocol);
            servicePanel1.setParams(m_protocol);

            historyDays.setParam(m_protocol, 128, Protocol.Commands.RD_DAILY_HISTORY, "day");
            historyMonth.setParam(m_protocol, 36, Protocol.Commands.RD_MONTH_HISTORY, "month");
            historyYear.setParam(m_protocol, 16, Protocol.Commands.RD_YEAR_HISTORY, "year");

            specDays1.setParent(this);

            boxNetAddr.Text = "0";

            refreshPorts();
            onClosePort();
        }



        private void refreshPorts() {
            string[] ports = SerialPort.GetPortNames();
            cmbCOMPort.Items.Clear();
            foreach (string port in ports) {
                cmbCOMPort.Items.Add(port);
            }
            if (cmbCOMPort.Items.Count > 0)
                cmbCOMPort.SelectedIndex = 0;
        }

        private void btnStart_Click(object sender, EventArgs e) {
            //m_protocol.setPort(cmbCOMPort.SelectedItem.ToString());
            try {
                m_serial.PortName = cmbCOMPort.SelectedItem.ToString();
                m_serial.BaudRate = 9600;
                m_serial.Parity = Parity.None;
                m_serial.DataBits = 8;
                m_serial.StopBits = StopBits.One;
                m_serial.Encoding = Encoding.UTF8;
                m_serial.Open();
                m_protocol.setPort(m_serial);
                UInt32 addr = 0;
                bool res = UInt32.TryParse(boxNetAddr.Text, out addr);
                if (res)
                    m_protocol.setNetAddr(addr);
                onOpenPort();

                if (comboUserAdmin.Text == "Пользователь") {
                    m_protocol.setPassword(0);
                }
                else {
                    // ntd
                }
            }
            catch {
                MessageBox.Show("Cant open");
            }
        }

        private void btnStop_Click(object sender, EventArgs e) {
            try {
                m_serial.Close();
                onClosePort();
            }
            catch {

            }
        }

        // visual states
        private void onOpenPort() {
            btnStart.Enabled = false;
            btnStop.Enabled = true;
        }
        private void onClosePort() {
            btnStart.Enabled = true;
            btnStop.Enabled = false;
        }


        private void btnRfsh_Click(object sender, EventArgs e) {
            refreshPorts();
        }



        class sCoverEv {
            public byte s;
            public byte m;
            public byte h;
            public byte d;
            public byte mn;
            public byte y;
            public byte code;
            static public int rawLen { get { return 7; } }
            public string getString() {
                string s = "The user {0} logged in";
                return s;
            }
            public byte[] asRaw {
                set {
                    s = value[0];
                    m = value[1];
                    h = value[2];
                    d = value[3];
                    mn = value[4];
                    y = value[5];
                    code = value[6];
                }
            }
        }

        
        

        private string formatUIetc(float val) {
            return String.Format("{0:0.00}", val);            
        }

        private void timer1_Tick(object sender, EventArgs e) {
            sRealMeasurements s = m_thread.getMeas();
            if (s != null) {
                viewMeasurements.Rows[(int)MEAS_ROW.U].Cells[1].Value = formatUIetc(s.curU);
                viewMeasurements.Rows[(int)MEAS_ROW.I].Cells[1].Value = formatUIetc(s.curI);
                viewMeasurements.Rows[(int)MEAS_ROW.P].Cells[1].Value = formatUIetc((float)(s.curP /1000.0));
                //viewMeasurements.Rows[(int)MEAS_ROW.Q].Cells[1].Value = formatUIetc((float)(s.curQ / 1000.0));
                viewMeasurements.Rows[(int)MEAS_ROW.F].Cells[1].Value = formatUIetc(s.freq);
                viewMeasurements.Rows[(int)MEAS_ROW.Ap].Cells[1].Value = formatUIetc(s.curEnergy / 1000);
                viewMeasurements.Rows[(int)MEAS_ROW.COS].Cells[1].Value = formatUIetc(s.cosFi);

            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e) {
            m_thread.stop();
            timer1.Stop();
            switch (treeView1.SelectedNode.Text) {
                case "Общие":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabCommon");
                    break;
                case "Служебные":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabService");                    
                    break;
                case "Текущие":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabCurrentEnergy");
                    break;
                case "Праздники":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabSpecDays");
                    break;
                case "Реле":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabRelay");
                    break;
                case "По дням":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabDailyHistory");
                    break;
                case "По месяцам":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabMonthlyHistory");
                    break;
                case "По годам":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabYearHistory");
                    break;

                case "Профиль мощности":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabDailyProfile");
                    break;
                case "Дисплей":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabDisplay");
                    break;
                case "Суточные графики":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabDailyGraphs");
                    break;
                case "Сезоны":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabSeasons");
                    break;
                case "Снятия крышки клеммной колодки":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabTerminalEv");
                    break;
                case "Воздействие магнитом":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabMagnetEv");
                    break;

                case "Изменения даты и времени":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabDTChangeEv");
                    break;
                case "Изменения тарифн. расписаний":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabTariffShegEv");
                    break;

                case "Изменения направления тока":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabCurrChangeEv");
                    break;

                case "Превышения лимита мощности":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabPowLimEv");
                    break;

                case "Превышения лимита энергии":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabEnLimEv");
                    break;

                case "Снятия крышки счетчика":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabCoverEv");
                    break;
                case "Вкл/Выкл. счетчика":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabPowerEv");
                    break;
                case "Параметры сети":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("measurements");
                    timer1.Start();
                    m_thread.start();
                    break;
                case "Дата и время":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("dateTime");
                    break;
                case "О счетчике":
                    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabAbout");
                    break;
                    
                //case "Журнал событий":
                //    tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabEvents");

                //    break;

                default:
                    //tabAll.SelectedIndex = tabAll.TabPages.IndexOfKey("tabAbout");
                    break;
            }

            

        }






        private void dailyPanel1_Load(object sender, EventArgs e) {

        }

        private void btnAddTime_Click(object sender, EventArgs e) {
            dailyPanel1.onAdd();
        }

        private void btnDelTime_Click(object sender, EventArgs e) {
            dailyPanel1.onDel();
        }




        private void btnAddSeas_Click(object sender, EventArgs e) {
            calendarPanel1.onAdd();
        }

        private void btnDelSeas_Click(object sender, EventArgs e) {
            calendarPanel1.onDel();
        }



        private void btnDel_Click(object sender, EventArgs e) {

        }

        private void btnAdd_Click(object sender, EventArgs e) {

        }

        private void boxPass_Enter(object sender, EventArgs e) {
            pass d = new pass();
            d.StartPosition = FormStartPosition.Manual;
            Point p = Cursor.Position;
            p.Offset(-20, 20);
            d.Location = p;

            DialogResult res = d.ShowDialog(this);
            if (res != DialogResult.OK) {
                boxPass.Text = "Нет";
                return;
            }

            password = d.getPass();
            m_protocol.setPassword(password);
            boxPass.Text = "Введен";
        }

        public bool canDelete(int num) {
            if (calendarPanel1.isUsed(num))
                return false;
            else
                return true;
        }

        public int getAvailableGraphs() {
            return dailyPanel1.Count();
        }

        

        public void ReadShedsSeasonsDays() {
            //TODO
            specDays1.ClearAll();
            dailyPanel1.ClearAll();
            calendarPanel1.ClearAll();

            byte[] arg = new byte[2];
            const byte maxSeas = 12;
            
            bool fin = false;
            List<List<CTariffIval>> lltarifs = new List<List<CTariffIval>>();

            //first get seasons
            byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.ENQ, (byte)Protocol.Commands.RD_TARIFF_SEASONS, Protocol.MAX_REPLYLEN, arg, 3000);
            if (reply == null)
                return;
            List<Date> lseas = CSeason.makeDatesFromByteArray(reply);
            for (int nSeas = 0; nSeas < lseas.Count; nSeas++) {
                CSeason ss = new CSeason();
                ss.start = lseas[nSeas];
                //if (ss == new CSeason(new Date(31, 12), new int[4]))
                //    break;
                if (ss.start.m_day == 31 && ss.start.m_month == 12)
                    break;

                ss.index = new int[4] { nSeas * 4, nSeas * 4 + 1, nSeas * 4 + 2, nSeas * 4 + 3 };
                calendarPanel1.onAdd(ss); // создали новый сезон, в котром четыре типа дней, они подряд отнумерованы 0,1,2,3 ... 4,5,6,7 etc
                //теперь надо вытащить сами графики
                byte[] recodeDayType = new byte[4] { 0, 3, 1, 2 }; // у нас они хранятся в порядке будние суббота воск праздничные. Но в протколе 0 - будние, 1 - воск 2 празд 3 субб            
                for (byte D = 0; D < 4; D++) {
                    arg[0] = (byte)nSeas;
                    arg[1] = recodeDayType[D];
                    byte[] reply2 = m_protocol.sendPacketGetReply(Protocol.Query.ENQ, (byte)Protocol.Commands.RD_TARIFF_SHEDS, Protocol.MAX_REPLYLEN, arg, 3000);
                    if (reply2 == null) {
                        fin = true;
                        break;
                    }
                    reply2 = reply2.Skip(2).ToArray();
                    List<CTariffIval> lstDays = DailyPanel.makeCTarifListFrombytes(reply2);
                    dailyPanel1.onAdd(lstDays); // добавляем новый график
                    lltarifs.Add(lstDays);                    
                }
            }
            // spec days

            byte[] reply3 = m_protocol.sendPacketGetReply(Protocol.Query.ENQ, (byte)Protocol.Commands.RD_TARIFF_SPEC_DAYS, Protocol.MAX_REPLYLEN, arg, 3000);
            if (reply3 == null)
                return;
            for (int i = 0; i < reply3.Count() / 2; i++) {
                CSpecdayInfo s = new CSpecdayInfo();

                s.m_date.m_day =  BCD.bcd2bin(reply3[i * 2]);
                s.m_date.m_month = BCD.bcd2bin(reply3[i * 2 + 1]) + 1;
                if (s.m_date.m_day == 0 )
                    break;
                specDays1.onAdd(s);
            }
        }
        public void WriteShedsSeasonsDays () {
            
            List<List<CTariffIval>> all = dailyPanel1.getAllTariifs();            
            List<CSeason> seasons = calendarPanel1.getAllSeasons();
            List <CSpecdayInfo> specDays =  specDays1.getAllDaysInfo();
            int replyOnLen = 4;
            byte[] recodeDayType = new byte[4] { 0, 3, 1, 2 }; // у нас они хранятся в порядке будние суббота воск праздничные. Но в протколе 0 - будние, 1 - воск 2 празд 3 субб            
            try {
                for (byte s = 0; s < seasons.Count; s++) {
                    for (byte i = 0; i < 4; i++) { // по типам дней                    
                        byte[] arg = new byte[256];
                        arg[0] = s;
                        arg[1] = recodeDayType[i];
                        int graphNum = seasons[s].index[i];
                        if (graphNum >= all.Count)
                            graphNum = 1;// it is error
                        List<CTariffIval> graph = all[graphNum];
                        Array arr = DailyPanel.makeByteArrayFromList_CTariffIval(graph);
                        Array.Copy(arr, 0, arg, 2, arr.Length);
                        Array.Resize(ref arg, arr.Length + 2);

                        
                        byte[] reply = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.WR_TARIFF_SHEDS, replyOnLen, arg, 3000);
                        if (reply == null)
                            throw new Exception("Не записано");
                        Thread.Sleep(100);
                    }
                }
                //seasons string
                List<byte> arrSeas = new List<byte>();
                for (byte i = 0; i < seasons.Count; i++) { // 
                    CSeason s = seasons[i];
                    arrSeas.AddRange(new byte[2] { (byte)s.start.m_day, (byte)(s.start.m_month - 1) }); // сезоны в графиках месяца нумеруют от 1, а в протоколе от 0
                }
                arrSeas.AddRange(new byte[2] { (byte)31, 11 });
                byte[] arg2 = new byte[256];
                Array.Copy(arrSeas.ToArray(), 0, arg2, 0, arrSeas.ToArray().Count());
                Array.Resize(ref arg2, arrSeas.ToArray().Count());
                arg2 = BCD.bin2bcd(arg2);
                byte[] reply2 = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.WR_TARIFF_SEASONS, replyOnLen, arg2, 3000);
                if (reply2 == null)
                    throw new Exception("Не записано");

                //spec days string
                List<byte> arrSpec = new List<byte>();
                for (byte i = 0; i < specDays.Count; i++) { // 
                    CSpecdayInfo s = specDays[i];
                    arrSpec.AddRange(new byte[2] { (byte)s.m_date.m_day, (byte)(s.m_date.m_month - 1) });
                }
                arrSpec.AddRange(new byte[2] { (byte)0, 0 });

                byte[] arg3 = new byte[256];
                Array.Copy(arrSpec.ToArray(), 0, arg3, 0, arrSpec.ToArray().Count());
                Array.Resize(ref arg3, arrSpec.ToArray().Count());
                arg3 = BCD.bin2bcd(arg3);
                byte[] reply3 = m_protocol.sendPacketGetReply(Protocol.Query.REC, (byte)Protocol.Commands.WR_TARIFF_SPEC_DAYS, replyOnLen, arg3, 3000);
                if (reply3 == null)
                    throw new Exception("Не записано");

                MessageBox.Show("Записано");

            }
            catch (Exception e) {
                MessageBox.Show(e.Message);
            }
        }

        class tabIDRDWR {
            public tabIDRDWR(string s, tabRDWRControl ev, Protocol.Commands cmd = Protocol.Commands.CMD_NONE, Protocol.Commands wrcmd = Protocol.Commands.CMD_NONE) {
                strTabName = s;
                list = ev;
                readCmd = cmd;
                m_wrCmd = wrcmd;
            }
            public string strTabName;
            public tabRDWRControl list;
            public Protocol.Commands readCmd;
            public Protocol.Commands m_wrCmd;
        }


        private tabIDRDWR[] getTable() {
            tabIDRDWR[] tabEventsDesc = new tabIDRDWR[] {

                new tabIDRDWR("tabAbout", about1),
            new tabIDRDWR("tabCurrentEnergy", currentEnergy1, Protocol.Commands.RD_CURR_EN),
            new tabIDRDWR("tabDailyGraphs", dailyPanel1, Protocol.Commands.RD_TARIFF_SHEDS, Protocol.Commands.WR_TARIFF_SHEDS),
            new tabIDRDWR("tabSeasons", calendarPanel1),
            new tabIDRDWR("tabSpecDays", specDays1),
            new tabIDRDWR("tabRelay", relay1, Protocol.Commands.WRRD_POWER_CONTROL, Protocol.Commands.WRRD_POWER_CONTROL),
            new tabIDRDWR("tabDisplay", displaySettings1, Protocol.Commands.WRRD_DISPLAY, Protocol.Commands.WRRD_DISPLAY  ),
            new tabIDRDWR("tabYearHistory", historyYear, Protocol.Commands.RD_YEAR_HISTORY ),
            new tabIDRDWR("tabMonthlyHistory", historyMonth, Protocol.Commands.RD_MONTH_HISTORY ),
            new tabIDRDWR("tabDailyHistory", historyDays, Protocol.Commands.RD_DAILY_HISTORY ),
            new tabIDRDWR("tabDailyProfile", dailyProfilePanel1, Protocol.Commands.RD_PROFILE_ANY_IDX ),
            new tabIDRDWR("tabCoverEv", eventListCover, Protocol.Commands.RD_COVER_EV ),
            new tabIDRDWR("tabPowerEv", eventListPower, Protocol.Commands.RD_POWER_EV ),
            new tabIDRDWR("tabMagnetEv", eventListMagnet , Protocol.Commands.RD_MAGNET_EV),
            new tabIDRDWR("tabTerminalEv", eventListTerminal, Protocol.Commands.RD_TERMINAL_EV),
            new tabIDRDWR("tabDTChangeEv", eventListDTChanged, Protocol.Commands.RD_DT_CHANGES),
            new tabIDRDWR("tabTariffShegEv", eventListShedEv, Protocol.Commands.RD_TARIFF_SHED_CH_EV),
            new tabIDRDWR("tabCurrChangeEv", eventListCurrChanged, Protocol.Commands.RD_CURR_EV_CH_EV),
            new tabIDRDWR("tabEnLimEv", eventListEnLim, Protocol.Commands.RD_ENERGY_LIM_EXC_EV),
            new tabIDRDWR("tabPowLimEv", eventListPowLim , Protocol.Commands.RD_POWER_LIM_EXC_EV),
            new tabIDRDWR("dateTime", settingsPanel1),
            
            };
            return tabEventsDesc;
        }

        private void toolBtnRead_Click(object sender, EventArgs e) {
            if (!m_serial.IsOpen)
                return;
            tabIDRDWR[] tabEventsDesc = getTable();

            foreach (tabIDRDWR i in tabEventsDesc) {
                if (tabAll.SelectedIndex == tabAll.TabPages.IndexOfKey(i.strTabName)) {
                    i.list.OnRead(m_protocol, i.readCmd);
                    break;
                }
            }

        }


        private void toolbtnAdd_Click(object sender, EventArgs e) {

            tabIDRDWR[] tabEventsDesc = getTable();
            foreach (tabIDRDWR i in tabEventsDesc) {
                if (tabAll.SelectedIndex == tabAll.TabPages.IndexOfKey(i.strTabName)) {
                    i.list.onAdd();
                    break;
                }
            }
        }

        private void toolBtnDel_Click(object sender, EventArgs e) {
            tabIDRDWR[] tabEventsDesc = getTable();
            foreach (tabIDRDWR i in tabEventsDesc) {
                if (tabAll.SelectedIndex == tabAll.TabPages.IndexOfKey(i.strTabName)) {
                    i.list.onDel();
                    break;
                }
            }
        }


        private void toolBtnWrite_Click(object sender, EventArgs e) {
            if (!m_serial.IsOpen)
                return;
            tabIDRDWR[] tabEventsDesc = getTable();

            foreach (tabIDRDWR i in tabEventsDesc) {
                if (tabAll.SelectedIndex == tabAll.TabPages.IndexOfKey(i.strTabName)) {
                    i.list.OnWrite(m_protocol, i.m_wrCmd);
                    break;
                }
            }
        }



        

        
        private void settingsPanel1_Load(object sender, EventArgs e) {

        }

        private void historyList1_Load(object sender, EventArgs e) {

        }


        private void toolBtnFromFile_Click(object sender, EventArgs e) {
            tabIDRDWR[] tabEventsDesc = getTable();
            foreach (tabIDRDWR i in tabEventsDesc) {
                if (tabAll.SelectedIndex == tabAll.TabPages.IndexOfKey(i.strTabName)) {
                    if (i.list.OnSaveToFile()) {
                        btnLoad_Click(sender, e);
                    }
                    break;
                }
            }
        }

        private void toolBtnToFile_Click(object sender, EventArgs e) {
            tabIDRDWR[] tabEventsDesc = getTable();
            foreach (tabIDRDWR i in tabEventsDesc) {
                if (tabAll.SelectedIndex == tabAll.TabPages.IndexOfKey(i.strTabName)) {
                    if (i.list.OnSaveToFile()) {
                        btnSave_Click(sender, e);
                    }
                    break;
                }
            }
        }

        private void about1_Load(object sender, EventArgs e) {

        }

        private void toolStripLabel3_Click(object sender, EventArgs e) {

        }

        private void boxPass_Click(object sender, EventArgs e) {

        }

        private void comboUserAdmin_Click(object sender, EventArgs e) {
            if (comboUserAdmin.Text == "Пользователь") {
                m_protocol.setPassword(0);
            }
            else {
                m_protocol.setPassword(0);
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e) {
            m_thread.stop();
        }

        private void currentEnergy1_Load(object sender, EventArgs e) {

        }
    }

    public class stringEv {
        public string dt;
        public string ev;
    }

}



