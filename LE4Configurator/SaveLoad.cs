using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Xml;
using System.Xml.Linq;

namespace LE4Configurator {
    public partial class Form1 : Form {
        private void btnSave_Click(object sender, EventArgs e) {
            FileDialog dlg = new SaveFileDialog();
            dlg.CheckFileExists = true;
            dlg.Filter = "Data Files (*.xml)|*.xml";
            dlg.DefaultExt = "xml";
            dlg.AddExtension = true;
            dlg.CheckFileExists = false;
            if (dlg.ShowDialog(this) != DialogResult.OK)
                return;
            XDocument xdoc = new XDocument();

            XElement xroot = new XElement("shedsettings");
            xdoc.Add(xroot);
            List<List<CTariffIval>> allsheds = dailyPanel1.getAllTariifs();
            for (int ii = 0; ii < allsheds.Count; ii++) {
                XElement graphEls = new XElement("graph");
                for (int i = 0; i < allsheds[ii].Count; i++) {
                    XElement x = new XElement("point");
                    x.Add(new XAttribute("number", i.ToString()));
                    x.Add(new XAttribute("time", allsheds[ii][i].tostringTime()));
                    x.Add(new XAttribute("tariff", allsheds[ii][i].tostringTariff()));
                    graphEls.Add(x);
                }
                xroot.Add(graphEls);
            }

            List<CSeason> seasList = calendarPanel1.getAllSeasons();
            for (int i = 0; i < seasList.Count; i++) {
                XElement seasEls = new XElement("Season");
                XElement x = new XElement("pointSeason");
                x.Add(new XAttribute("start", seasList[i].toXmlString()));
                x.Add(new XAttribute("D0", seasList[i].index[0].ToString()));
                x.Add(new XAttribute("D1", seasList[i].index[1].ToString()));
                x.Add(new XAttribute("D2", seasList[i].index[2].ToString()));
                x.Add(new XAttribute("D3", seasList[i].index[3].ToString()));
                seasEls.Add(x);
                xroot.Add(seasEls);
            }


            List<CSpecdayInfo> specdays = specDays1.getSpecDays();
            for (int i = 0; i < specdays.Count; i++) {
                XElement specEls = new XElement("SpecDay");
                specEls.Add(new XAttribute("day", specdays[i].toXmlString()));                
                xroot.Add(specEls);
            }

            allDispSettings all = displaySettings1.getAllSettings();
            XElement dSettings = new XElement("dispSettings");
            for (int i = 0; i < all.onOffList.Count; i++) {
                dSettings.Add(new XAttribute(all.onOffList[i].name.Replace(" ", "_"), all.onOffList[i].isOn.ToString().ToLower()));
            }
            dSettings.Add(new XAttribute("time", all.time.ToString()));
            xroot.Add(dSettings);
            xdoc.Save(dlg.FileName);
        }

        private void btnSaveSeas_Click(object sender, EventArgs e) {
            btnSave_Click(sender, e);
        }


        private void btnLoad_Click(object sender, EventArgs e) {

            FileDialog dlg = new OpenFileDialog();
            dlg.CheckFileExists = true;
            dlg.Filter = "Data Files (*.xml)|*.xml";
            dlg.DefaultExt = "xml";
            dlg.AddExtension = true;
            if (dlg.ShowDialog(this) != DialogResult.OK)
                return;
            dailyPanel1.ClearAll();

            List<List<CTariffIval>> list = new List<List<CTariffIval>>();
            XDocument x = XDocument.Load(dlg.FileName);
            XElement root = x.Root;
            IEnumerable<XElement> el =
                from nd in root.Elements()
                where nd.Name.LocalName == "graph"
                select nd;
            foreach (XElement elem in el) { // here graps
                IEnumerable<XElement> elpoint =
                    from nd in elem.Elements()
                    where nd.Name.LocalName == "point"
                    select nd;

                List<CTariffIval> listOne = new List<CTariffIval>();
                foreach (XElement xe1 in elpoint) {
                    listOne.Add(CTariffIval.constructFromStr(xe1.Attribute("time").Value, xe1.Attribute("tariff").Value));
                }
                dailyPanel1.onAdd(listOne);
            }

            calendarPanel1.ClearAll();
            IEnumerable<XElement> elSeas =
                from nd in root.Elements()
                where nd.Name.LocalName == "Season"
                select nd;
            foreach (XElement elem2 in elSeas) { // here seas
                IEnumerable<XElement> elpointSeas =
                    from nd in elem2.Elements()
                    where nd.Name.LocalName == "pointSeason"
                    select nd;
                foreach (XElement xe in elpointSeas) {
                    CSeason s = CSeason.constructFromStr(xe.Attribute("start").Value, xe.Attribute("D0").Value, xe.Attribute("D1").Value, xe.Attribute("D2").Value, xe.Attribute("D3").Value);
                    calendarPanel1.onAdd(s);
                    
                }

            }

            specDays1.ClearAll();
            IEnumerable<XElement> elSpec =
                from nd in root.Elements()
                where nd.Name.LocalName == "SpecDay"
                select nd;
            foreach (XElement elem3 in elSpec) { // here spec days
                CSpecdayInfo inf = CSpecdayInfo.constructFromStr(elem3.Attribute("day").Value);                
                specDays1.onAdd(inf);
            }

            IEnumerable<XElement> elDisp =
                from nd in root.Elements()
                where nd.Name.LocalName == "dispSettings"
                select nd;
            if (elDisp != null) {
                allDispSettings all = new allDispSettings();
                List<sDisplaySetting> l = new List<sDisplaySetting>();
                all.onOffList = l;
                for (int i = 0; i <  DisplaySettings.settings.Count; i++) {
                    string sd = elDisp.First().Attribute(DisplaySettings.settings[i].name.Replace(" ", "_")).Value;
                    l.Add(new sDisplaySetting(DisplaySettings.settings[i].name, sd == "true"));    
                }
                Int32 t = 0;                
                Int32.TryParse(elDisp.First().Attribute("time").Value, out t);
                all.time = t;
                displaySettings1.initFromStruct(all);
            }
        }
    }
}