﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class Relay : tabRDWRControl {
        public Relay() {
            InitializeComponent();
        }

        public override void OnRead(Protocol proto, Protocol.Commands cmd) {
            byte[] reply = proto.sendPacketGetReply(Protocol.Query.ENQ, (byte)cmd, Protocol.MAX_REPLYLEN);
            if (reply == null) {
                return;
            }
            int idx = 0;

            checkPowLim.Checked =  BitConverter.ToBoolean(reply, idx); idx++;

            boxPower.Text = BitConverter.ToUInt32(reply, idx).ToString(); idx += 4;
            boxAboveTime.Text = BitConverter.ToUInt32(reply, idx).ToString(); idx += 4;
            boxOffTime.Text = (BitConverter.ToUInt32(reply, idx) / 60).ToString(); idx += 4;          

            chkLimEn1.Checked = BitConverter.ToBoolean(reply, idx); idx++;
            chkLimEn2.Checked = BitConverter.ToBoolean(reply, idx); idx++;
            chkLimEn3.Checked = BitConverter.ToBoolean(reply, idx); idx++;
            chkLimEn4.Checked = BitConverter.ToBoolean(reply, idx); idx++;

            boxLimEn1.Text = BitConverter.ToUInt32(reply, idx).ToString(); idx += 4;
            boxLimEn2.Text = BitConverter.ToUInt32(reply, idx).ToString(); idx += 4;
            boxLimEn3.Text = BitConverter.ToUInt32(reply, idx).ToString(); idx += 4;
            boxLimEn4.Text = BitConverter.ToUInt32(reply, idx).ToString(); idx += 4;
        }


        public override void OnWrite(Protocol proto, Protocol.Commands cmd) {
            UInt32 aboveTime = 0;
            UInt32 offTime = 0;
            UInt32 power = 0;
            UInt32.TryParse(boxAboveTime.Text, out aboveTime);
            UInt32.TryParse(boxOffTime.Text, out offTime); offTime = offTime * 60;
            UInt32.TryParse(boxPower.Text, out power);
            bool ison = checkPowLim.Checked;
            List<byte> packList = new List<byte>();
            packList.AddRange(BitConverter.GetBytes(ison));
            packList.AddRange(BitConverter.GetBytes(power));
            packList.AddRange(BitConverter.GetBytes(aboveTime));
            packList.AddRange(BitConverter.GetBytes(offTime));

            UInt32 t1lev = 0;
            UInt32 t2lev = 0;
            UInt32 t3lev = 0;
            UInt32 t4lev = 0;
            UInt32.TryParse(boxLimEn1.Text, out t1lev);
            UInt32.TryParse(boxLimEn2.Text, out t2lev);
            UInt32.TryParse(boxLimEn3.Text, out t3lev);
            UInt32.TryParse(boxLimEn4.Text, out t4lev);
            bool is1 = false; is1 = chkLimEn1.Checked;
            bool is2 = false; is2 = chkLimEn2.Checked;
            bool is3 = false; is3 = chkLimEn3.Checked;
            bool is4 = false; is4 = chkLimEn4.Checked;
            
            packList.AddRange(BitConverter.GetBytes(is1));
            packList.AddRange(BitConverter.GetBytes(is2));
            packList.AddRange(BitConverter.GetBytes(is3));
            packList.AddRange(BitConverter.GetBytes(is4));
            packList.AddRange(BitConverter.GetBytes(t1lev));
            packList.AddRange(BitConverter.GetBytes(t2lev));
            packList.AddRange(BitConverter.GetBytes(t3lev));
            packList.AddRange(BitConverter.GetBytes(t4lev));

            byte[] reply = proto.sendPacketGetReply(Protocol.Query.REC, (byte)cmd, Protocol.MAX_REPLYLEN, packList.ToArray());
            if (reply == null) {
                MessageBox.Show("Не могу записать");
                return;
            }
            MessageBox.Show("Записано");
        }

        private void btnOnRelay_Click(object sender, EventArgs e) {

        }

    }
}
