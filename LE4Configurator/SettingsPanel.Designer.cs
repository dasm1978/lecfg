﻿namespace LE4Configurator {
    partial class SettingsPanel {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.gpbx1 = new System.Windows.Forms.GroupBox();
            this.dateTimePickerControl = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.chkTimeCurrent = new System.Windows.Forms.CheckBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gpbx1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpbx1
            // 
            this.gpbx1.Controls.Add(this.label2);
            this.gpbx1.Controls.Add(this.comboBox1);
            this.gpbx1.Controls.Add(this.dateTimePickerControl);
            this.gpbx1.Controls.Add(this.label1);
            this.gpbx1.Controls.Add(this.chkTimeCurrent);
            this.gpbx1.Controls.Add(this.dateTimePicker1);
            this.gpbx1.Location = new System.Drawing.Point(27, 34);
            this.gpbx1.Name = "gpbx1";
            this.gpbx1.Size = new System.Drawing.Size(411, 135);
            this.gpbx1.TabIndex = 3;
            this.gpbx1.TabStop = false;
            this.gpbx1.Text = "Установка времени";
            // 
            // dateTimePickerControl
            // 
            this.dateTimePickerControl.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerControl.Location = new System.Drawing.Point(19, 87);
            this.dateTimePickerControl.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePickerControl.Name = "dateTimePickerControl";
            this.dateTimePickerControl.Size = new System.Drawing.Size(135, 20);
            this.dateTimePickerControl.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 72);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Время счетчика";
            // 
            // chkTimeCurrent
            // 
            this.chkTimeCurrent.AutoSize = true;
            this.chkTimeCurrent.Location = new System.Drawing.Point(334, 37);
            this.chkTimeCurrent.Name = "chkTimeCurrent";
            this.chkTimeCurrent.Size = new System.Drawing.Size(71, 17);
            this.chkTimeCurrent.TabIndex = 2;
            this.chkTimeCurrent.Text = "Текущее";
            this.chkTimeCurrent.UseVisualStyleBackColor = true;
            this.chkTimeCurrent.CheckedChanged += new System.EventHandler(this.chkTimeCurrent_CheckedChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(19, 33);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 0;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(254, 33);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(63, 21);
            this.comboBox1.TabIndex = 6;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(225, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "+";
            // 
            // SettingsPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gpbx1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SettingsPanel";
            this.Size = new System.Drawing.Size(1056, 711);
            this.gpbx1.ResumeLayout(false);
            this.gpbx1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbx1;
        private System.Windows.Forms.CheckBox chkTimeCurrent;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePickerControl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
    }
}
