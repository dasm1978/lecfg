﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace LE4Configurator {
    public class Protocol {
        public enum Query { ENQ = 1, REC = 3, DRJ = 0x0a };
        //const int WR_ACCESS = 
        public enum Commands {
            RD_CURR_EN = 0x03,
            RD_PROFILE_IDX = 0x16, RD_DT_CHANGES = 0x17, RD_COVER_EV = 0x30, RD_POWER_EV = 0x31,
            RD_TERMINAL_EV = 0x32, RD_MAGNET_EV = 0x33, RD_TARIFF_SHED_CH_EV = 0x34,
            RD_CURR_EV_CH_EV = 0x35,
            RD_POWER_LIM_EXC_EV = 0x36,
            RD_ENERGY_LIM_EXC_EV = 0x37,
            WRRD_POWER_CONTROL = 0x1a,
            RD_CALIBRATION = 0xcb,
            WR_CALIBRATION = 0xcb,
            WR_RTC_COEFF = 0xcd,
            RD_RTC_COEFF = 0xcd,
            WR_R = 0xce,
            EX_CLR_JRNLS = 0xd0,
            EX_CLR_POW_PROFILES = 0xd1,
            EX_CLR_ENERGY = 0xd2,
            EX_CLR_ERRS = 0xd3,


            RD_PROFILE_TIME = 0xe0,
            WR_PROFILE_TIME = 0xe0,

            RD_COUNTER_INFO = 0xf0,
            WR_COUNTER_INFO = 0xf0,

            EXEC_PPS_ON = 0xe5,

            RD_MEASURE = 0x40, RD_PROFILE_ANY_IDX = 0x50,
            RD_TARIFF_SPEC_DAYS = 0x09, WR_TARIFF_SPEC_DAYS = 0x09, RD_TARIFF_SEASONS = 0x0a, WR_TARIFF_SEASONS = 0x0a, RD_TARIFF_SHEDS = 0x0b, WR_TARIFF_SHEDS = 0x0b,
            RD_DAILY_HISTORY = 0x0d,
            RD_MONTH_HISTORY = 0x0e,
            RD_YEAR_HISTORY = 0x0f,
            WR_DTIME = 0,
            WRRD_DISPLAY = 8,
            WR_PASSWORD = 0x0d,

            CMD_NONE = 0xffff
        };

        const int START_MARK = 0x02;
        const int SER_READ_TIMEOUT = 50;
        const int DEFAULT_PACKET_TIMEOUT = 500;
        const int DEFAULT_MAX_REPLY_PACKET_LEN = 256;

        private UInt32 defaultPassword = 0x0001b207;
        private UInt32 defaultAddr = 0x0;
        public void setPort(SerialPort port) {
            m_serial = port;
        }
        public bool isOpen() {
            return m_serial.IsOpen;
        }
        //вернет массив с телом ответа (без фильтрации по по сетевому адресу
        // параметр - ожидаемая длина тела ответа
        // если принятный пакет содержит меньшую длину, чем ожидаемую - вернет массив с реальной длиной ответа (может быть меньше
        // запрашиваемой. Если равную или большую - усечет до размеров ожидаемой длины
        // таймаут тут - междубайтный, для разделения пакетов . 




        private byte[] readPacketBody(int expectBodyLen, int timeout) {
            byte[] body = new byte[expectBodyLen];
            int infoLen = 1 + 1 + 4 + 4 + 1 + 1 + 2;
            int bodyOffset = infoLen - 2; // except crc
            int fullLen = expectBodyLen + infoLen;
            byte[] rawReply = readTimeOut(fullLen, timeout); // full packet

            int realBodyLen = 0;
            if (rawReply.Length < infoLen)
                return null;
            if (rawReply[10] == 0x0a) {
                if (rawReply[11] == 0x02) {
                    MessageBox.Show("Неверный пароль");
                    throw new Exception();
                    return null;
                }
                else if (rawReply[11] == 0x03) {
                    MessageBox.Show("Не установлен джампер");
                    throw new Exception();
                    return null;
                }
                else if (rawReply[11] == 0xa) { // это ошибка "нет записей" - выдаем ее за то, что считывание закончена=о
                    MessageBox.Show("OK");
                }

            }

            realBodyLen = rawReply.Length - infoLen;
            if (realBodyLen > expectBodyLen)
                realBodyLen = expectBodyLen;
            Array.Copy(rawReply, bodyOffset, body, 0, realBodyLen);
            Array.Resize(ref body, realBodyLen);
            return body;
        }

        private void sendPacket(byte[] data) {
            try {
                if (data.Length > 255 - 2 - 2)
                    return;
                byte dataLen = (byte)data.Length;
                byte[] tosend = new byte[dataLen + 2 + 2];
                tosend[0] = START_MARK;
                tosend[1] = (byte)(dataLen + 2 + 2);
                Array.Copy(data, 0, tosend, 2, dataLen);
                ushort crc = crc16.crc16calc(tosend, 0, tosend.Length - 2);
                tosend[tosend.Length - 2] = (byte)crc;
                tosend[tosend.Length - 1] = (byte)(crc >> 8);
                m_serial.DiscardInBuffer();
                m_serial.Write(tosend, 0, tosend.Length);
            }
            catch {
                //MessageBox.Show("port closed");

            }
        }

        private void sendPacket(Query com, byte id, byte[] data = null, netAddress a = null, password p = null) {
            const int offs_data = 10; // смещение доп данных в пакете
            byte[] b = new byte[256];
            if (a == null)
                a = new netAddress(defaultAddr);
            Array.Copy(a.addr, b, a.addr.Length);
            if (p == null)
                p = new password(defaultPassword);
            Array.Copy(p.raw, 0, b, a.addr.Length, p.raw.Length);
            b[8] = (byte)com;
            b[9] = (byte)id;
            if (data != null)
                Array.Copy(data, 0, b, offs_data, data.Length); //
            if (data != null)
                Array.Resize(ref b, data.Length + offs_data);
            else
                Array.Resize(ref b, 0 + offs_data);
            sendPacket(b);
        }

        public const int MAX_REPLYLEN = 255;
        private UInt32 m_password = 0;
        private UInt32 m_netAddr = 0;
        public void setPassword(UInt32 pass) {
            m_password = pass;
        }

        public void setNetAddr(UInt32 addr) {
            m_netAddr = addr;
        }
        public byte[] sendPacketGetReply(Query com, byte id, int replyLen = MAX_REPLYLEN, byte[] packetData = null, int timeout = DEFAULT_PACKET_TIMEOUT, netAddress a = null, password p = null) {
            const int maxtry = 3;
            int retry = 0;
            byte[] bd = null;

            p = new password(m_password);

            if (a == null) {
                a = new netAddress(m_netAddr);
            }
            try {
                for (int i = 0; i < maxtry; i++) {
                    sendPacket(com, id, packetData, a, p);
                    bd = readReply(replyLen, timeout);
                    if (bd != null)
                        break;
                    else {
                        retry++;
                    }
                    Thread.Sleep(100);
                }
            }
            catch {
                return null;
            }
            return bd;
        }


        public static byte tocmd(Commands c) {
            return (byte)((byte)c & 0xff);
        }


        public byte[] readReply(int expectBodyLen, int timeout = DEFAULT_PACKET_TIMEOUT) {
            byte[] reply = null;
            try {
                long timeEnd = DateTime.Now.Ticks + TimeSpan.TicksPerMillisecond * timeout;
                
                do {
                    reply = readPacketBody(expectBodyLen, SER_READ_TIMEOUT);
                    if (reply != null)
                        break;
                } while (DateTime.Now.Ticks <= timeEnd);
            }
            catch (Exception e) {
                throw (e);
                return null;
            }
            return reply;
        }

        // чтение на низком уровне, читает все, если получено меньше, чем ожидали - вернет меньший размер
        private byte[] readTimeOut(int count, int timeout) {
            byte[] buffer = new byte[count];
            m_serial.ReadTimeout = timeout;
            int ofs = 0;
            try {
                while (ofs < count) {
                    buffer[ofs] = (byte)(m_serial.ReadByte());
                    ofs++;
                }
            }
            catch (TimeoutException e) {

            }
            catch {

            }

            Array.Resize(ref buffer, ofs);
            return buffer;
        }
        private System.IO.Ports.SerialPort m_serial = new SerialPort();
    }
    public class netAddress {
        public netAddress(UInt32 a) {
            addr = new byte[4];
            addr[0] = (byte)a;
            addr[1] = (byte)(a >> 8);
            addr[2] = (byte)(a >> 16);
            addr[3] = (byte)(a >> 24);
        }

        public netAddress(string s) {

        }

        public byte[] addr { set; get; }
    }
    public class password {
        public password(UInt32 a) {
            raw = new byte[4];
            raw[0] = (byte)a;
            raw[1] = (byte)(a >> 8);
            raw[2] = (byte)(a >> 16);
            raw[3] = (byte)(a >> 24);
        }
        public byte[] raw { set; get; }
    }
}

public class crc16 {
    static public ushort crc16calc(byte[] b) {
        return crc16calc(b, 0, b.Length);
    }
    static public ushort crc16calc(byte[] arr, int offest, int length) {
        if (offest >= length) throw new Exception();
        byte[] b = new byte[length - offest];
        Array.Copy(arr, offest, b, 0, length);
        byte d = 0;
        int idx = 0;
        int len = b.Length;
        ushort crc = 0xffff;
        do {
            d = (byte)(b[idx++] ^ (byte)crc);
            d ^= (byte)(d << 4);
            crc = (ushort)((d << 3) ^ (d << 8) ^ (crc >> 8) ^ (d >> 4));
        }
        while (--len > 0);
        crc ^= 0xffff;
        return crc;
    }

}
