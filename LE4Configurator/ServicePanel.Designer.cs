﻿namespace LE4Configurator {
    partial class ServicePanel {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnWrProfIval = new System.Windows.Forms.Button();
            this.upDwnRecIval = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnClrJournals = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnPPSOff = new System.Windows.Forms.Button();
            this.btnPPSOn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnReadKClock = new System.Windows.Forms.Button();
            this.btnWRKClock = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnReadCal = new System.Windows.Forms.Button();
            this.btnWrCal = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.boxkQ = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.boxkP = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.boxkI = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.boxkU = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnClrPowerProfile = new System.Windows.Forms.Button();
            this.btnClearAccumulatedEnergy = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnClrErr = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btnWrK = new System.Windows.Forms.Button();
            this.numK = new System.Windows.Forms.NumericUpDown();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.btnCalibr = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.boxRealPQ = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.boxRealP = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.boxRealI = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.boxRealU = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.upDwnRecIval)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numK)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnWrProfIval
            // 
            this.btnWrProfIval.Location = new System.Drawing.Point(184, 34);
            this.btnWrProfIval.Name = "btnWrProfIval";
            this.btnWrProfIval.Size = new System.Drawing.Size(75, 23);
            this.btnWrProfIval.TabIndex = 0;
            this.btnWrProfIval.Text = "Записать";
            this.btnWrProfIval.UseVisualStyleBackColor = true;
            this.btnWrProfIval.Click += new System.EventHandler(this.btnWrProfIval_Click);
            // 
            // upDwnRecIval
            // 
            this.upDwnRecIval.Location = new System.Drawing.Point(22, 34);
            this.upDwnRecIval.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.upDwnRecIval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.upDwnRecIval.Name = "upDwnRecIval";
            this.upDwnRecIval.Size = new System.Drawing.Size(120, 20);
            this.upDwnRecIval.TabIndex = 1;
            this.upDwnRecIval.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.upDwnRecIval);
            this.groupBox1.Controls.Add(this.btnWrProfIval);
            this.groupBox1.Location = new System.Drawing.Point(16, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(278, 86);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Интервал записи профиля, мин";
            // 
            // btnClrJournals
            // 
            this.btnClrJournals.Location = new System.Drawing.Point(22, 35);
            this.btnClrJournals.Name = "btnClrJournals";
            this.btnClrJournals.Size = new System.Drawing.Size(75, 23);
            this.btnClrJournals.TabIndex = 4;
            this.btnClrJournals.Text = "Очистка журналов";
            this.btnClrJournals.UseVisualStyleBackColor = true;
            this.btnClrJournals.Click += new System.EventHandler(this.btnClrJournals_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnClrJournals);
            this.groupBox2.Location = new System.Drawing.Point(16, 147);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(278, 100);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Журналы событий";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnPPSOff);
            this.groupBox3.Controls.Add(this.btnPPSOn);
            this.groupBox3.Location = new System.Drawing.Point(16, 279);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(278, 100);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "PPS";
            // 
            // btnPPSOff
            // 
            this.btnPPSOff.Location = new System.Drawing.Point(184, 41);
            this.btnPPSOff.Name = "btnPPSOff";
            this.btnPPSOff.Size = new System.Drawing.Size(75, 23);
            this.btnPPSOff.TabIndex = 6;
            this.btnPPSOff.Text = "Off";
            this.btnPPSOff.UseVisualStyleBackColor = true;
            this.btnPPSOff.Click += new System.EventHandler(this.btnPPSOff_Click);
            // 
            // btnPPSOn
            // 
            this.btnPPSOn.Location = new System.Drawing.Point(22, 41);
            this.btnPPSOn.Name = "btnPPSOn";
            this.btnPPSOn.Size = new System.Drawing.Size(75, 23);
            this.btnPPSOn.TabIndex = 5;
            this.btnPPSOn.Text = "On";
            this.btnPPSOn.UseVisualStyleBackColor = true;
            this.btnPPSOn.Click += new System.EventHandler(this.btnPPSOn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnReadKClock);
            this.groupBox4.Controls.Add(this.btnWRKClock);
            this.groupBox4.Controls.Add(this.numericUpDown1);
            this.groupBox4.Location = new System.Drawing.Point(343, 279);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(238, 100);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Коэфф. часов";
            // 
            // btnReadKClock
            // 
            this.btnReadKClock.Location = new System.Drawing.Point(141, 19);
            this.btnReadKClock.Name = "btnReadKClock";
            this.btnReadKClock.Size = new System.Drawing.Size(75, 23);
            this.btnReadKClock.TabIndex = 2;
            this.btnReadKClock.Text = "Прочесть";
            this.btnReadKClock.UseVisualStyleBackColor = true;
            this.btnReadKClock.Click += new System.EventHandler(this.btnReadKClock_Click);
            // 
            // btnWRKClock
            // 
            this.btnWRKClock.Location = new System.Drawing.Point(141, 61);
            this.btnWRKClock.Name = "btnWRKClock";
            this.btnWRKClock.Size = new System.Drawing.Size(75, 23);
            this.btnWRKClock.TabIndex = 1;
            this.btnWRKClock.Text = "Записать";
            this.btnWRKClock.UseVisualStyleBackColor = true;
            this.btnWRKClock.Click += new System.EventHandler(this.btnWRKClock_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(6, 19);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            8191,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            8191,
            0,
            0,
            -2147483648});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(97, 20);
            this.numericUpDown1.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnReadCal);
            this.groupBox5.Controls.Add(this.btnWrCal);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.boxkQ);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.boxkP);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.boxkI);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.boxkU);
            this.groupBox5.Location = new System.Drawing.Point(343, 39);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(238, 208);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Калибровка";
            // 
            // btnReadCal
            // 
            this.btnReadCal.Location = new System.Drawing.Point(141, 36);
            this.btnReadCal.Name = "btnReadCal";
            this.btnReadCal.Size = new System.Drawing.Size(75, 23);
            this.btnReadCal.TabIndex = 4;
            this.btnReadCal.Text = "Прочесть";
            this.btnReadCal.UseVisualStyleBackColor = true;
            this.btnReadCal.Click += new System.EventHandler(this.btnReadCal_Click);
            // 
            // btnWrCal
            // 
            this.btnWrCal.Location = new System.Drawing.Point(141, 78);
            this.btnWrCal.Name = "btnWrCal";
            this.btnWrCal.Size = new System.Drawing.Size(75, 23);
            this.btnWrCal.TabIndex = 3;
            this.btnWrCal.Text = "Записать";
            this.btnWrCal.UseVisualStyleBackColor = true;
            this.btnWrCal.Click += new System.EventHandler(this.btnWrCal_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "kQ";
            // 
            // boxkQ
            // 
            this.boxkQ.Location = new System.Drawing.Point(6, 168);
            this.boxkQ.Name = "boxkQ";
            this.boxkQ.Size = new System.Drawing.Size(100, 20);
            this.boxkQ.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "kP";
            // 
            // boxkP
            // 
            this.boxkP.Location = new System.Drawing.Point(6, 124);
            this.boxkP.Name = "boxkP";
            this.boxkP.Size = new System.Drawing.Size(100, 20);
            this.boxkP.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "kI";
            // 
            // boxkI
            // 
            this.boxkI.Location = new System.Drawing.Point(6, 80);
            this.boxkI.Name = "boxkI";
            this.boxkI.Size = new System.Drawing.Size(100, 20);
            this.boxkI.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "kU";
            // 
            // boxkU
            // 
            this.boxkU.Location = new System.Drawing.Point(6, 36);
            this.boxkU.Name = "boxkU";
            this.boxkU.Size = new System.Drawing.Size(100, 20);
            this.boxkU.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnClrPowerProfile);
            this.groupBox6.Location = new System.Drawing.Point(16, 414);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(278, 68);
            this.groupBox6.TabIndex = 9;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Профиль мощности";
            // 
            // btnClrPowerProfile
            // 
            this.btnClrPowerProfile.Location = new System.Drawing.Point(22, 35);
            this.btnClrPowerProfile.Name = "btnClrPowerProfile";
            this.btnClrPowerProfile.Size = new System.Drawing.Size(75, 23);
            this.btnClrPowerProfile.TabIndex = 4;
            this.btnClrPowerProfile.Text = "Очистка журналов";
            this.btnClrPowerProfile.UseVisualStyleBackColor = true;
            this.btnClrPowerProfile.Click += new System.EventHandler(this.btnClrPowerProfile_Click);
            // 
            // btnClearAccumulatedEnergy
            // 
            this.btnClearAccumulatedEnergy.Location = new System.Drawing.Point(22, 35);
            this.btnClearAccumulatedEnergy.Name = "btnClearAccumulatedEnergy";
            this.btnClearAccumulatedEnergy.Size = new System.Drawing.Size(75, 23);
            this.btnClearAccumulatedEnergy.TabIndex = 4;
            this.btnClearAccumulatedEnergy.Text = "Очистка журналов";
            this.btnClearAccumulatedEnergy.UseVisualStyleBackColor = true;
            this.btnClearAccumulatedEnergy.Click += new System.EventHandler(this.btnClearAccumulatedEnergy_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btnClearAccumulatedEnergy);
            this.groupBox7.Location = new System.Drawing.Point(343, 414);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(238, 68);
            this.groupBox7.TabIndex = 10;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Накопления";
            // 
            // btnClrErr
            // 
            this.btnClrErr.Location = new System.Drawing.Point(22, 35);
            this.btnClrErr.Name = "btnClrErr";
            this.btnClrErr.Size = new System.Drawing.Size(75, 23);
            this.btnClrErr.TabIndex = 4;
            this.btnClrErr.Text = "Очистка журналов";
            this.btnClrErr.UseVisualStyleBackColor = true;
            this.btnClrErr.Click += new System.EventHandler(this.btnClrErr_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.btnClrErr);
            this.groupBox8.Location = new System.Drawing.Point(633, 414);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(238, 68);
            this.groupBox8.TabIndex = 11;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Ошибки";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.btnWrK);
            this.groupBox9.Controls.Add(this.numK);
            this.groupBox9.Location = new System.Drawing.Point(633, 279);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(238, 100);
            this.groupBox9.TabIndex = 8;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Коэфф. счетчика";
            // 
            // btnWrK
            // 
            this.btnWrK.Location = new System.Drawing.Point(143, 19);
            this.btnWrK.Name = "btnWrK";
            this.btnWrK.Size = new System.Drawing.Size(75, 23);
            this.btnWrK.TabIndex = 1;
            this.btnWrK.Text = "Записать";
            this.btnWrK.UseVisualStyleBackColor = true;
            this.btnWrK.Click += new System.EventHandler(this.btnWrK_Click);
            // 
            // numK
            // 
            this.numK.Location = new System.Drawing.Point(6, 19);
            this.numK.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numK.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numK.Name = "numK";
            this.numK.Size = new System.Drawing.Size(97, 20);
            this.numK.TabIndex = 0;
            this.numK.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.btnCalibr);
            this.groupBox10.Controls.Add(this.label5);
            this.groupBox10.Controls.Add(this.boxRealPQ);
            this.groupBox10.Controls.Add(this.label6);
            this.groupBox10.Controls.Add(this.boxRealP);
            this.groupBox10.Controls.Add(this.label7);
            this.groupBox10.Controls.Add(this.boxRealI);
            this.groupBox10.Controls.Add(this.label8);
            this.groupBox10.Controls.Add(this.boxRealU);
            this.groupBox10.Location = new System.Drawing.Point(633, 39);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(238, 208);
            this.groupBox10.TabIndex = 9;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Целевые";
            // 
            // btnCalibr
            // 
            this.btnCalibr.Location = new System.Drawing.Point(143, 34);
            this.btnCalibr.Name = "btnCalibr";
            this.btnCalibr.Size = new System.Drawing.Size(75, 23);
            this.btnCalibr.TabIndex = 3;
            this.btnCalibr.Text = "Калибр";
            this.btnCalibr.UseVisualStyleBackColor = true;
            this.btnCalibr.Click += new System.EventHandler(this.btnCalibr_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Q";
            // 
            // boxRealPQ
            // 
            this.boxRealPQ.Location = new System.Drawing.Point(6, 168);
            this.boxRealPQ.Name = "boxRealPQ";
            this.boxRealPQ.Size = new System.Drawing.Size(100, 20);
            this.boxRealPQ.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "P";
            // 
            // boxRealP
            // 
            this.boxRealP.Location = new System.Drawing.Point(6, 124);
            this.boxRealP.Name = "boxRealP";
            this.boxRealP.Size = new System.Drawing.Size(100, 20);
            this.boxRealP.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "I";
            // 
            // boxRealI
            // 
            this.boxRealI.Location = new System.Drawing.Point(6, 80);
            this.boxRealI.Name = "boxRealI";
            this.boxRealI.Size = new System.Drawing.Size(100, 20);
            this.boxRealI.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "U";
            // 
            // boxRealU
            // 
            this.boxRealU.Location = new System.Drawing.Point(6, 36);
            this.boxRealU.Name = "boxRealU";
            this.boxRealU.Size = new System.Drawing.Size(100, 20);
            this.boxRealU.TabIndex = 0;
            // 
            // ServicePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ServicePanel";
            this.Size = new System.Drawing.Size(1240, 624);
            ((System.ComponentModel.ISupportInitialize)(this.upDwnRecIval)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numK)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnWrProfIval;
        private System.Windows.Forms.NumericUpDown upDwnRecIval;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnClrJournals;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnPPSOff;
        private System.Windows.Forms.Button btnPPSOn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnReadKClock;
        private System.Windows.Forms.Button btnWRKClock;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnReadCal;
        private System.Windows.Forms.Button btnWrCal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox boxkQ;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox boxkP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox boxkI;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox boxkU;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnClrPowerProfile;
        private System.Windows.Forms.Button btnClearAccumulatedEnergy;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btnClrErr;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button btnWrK;
        private System.Windows.Forms.NumericUpDown numK;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button btnCalibr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox boxRealPQ;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox boxRealP;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox boxRealI;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox boxRealU;
    }
}
