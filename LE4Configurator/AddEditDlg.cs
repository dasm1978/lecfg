﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LE4Configurator {
    public partial class AddEditDlg : Form
    {
        public AddEditDlg(CTariffIval tmBegin, List<eTariff>  tar)
        {
            InitializeComponent();
            pickTimeEnd.Format = DateTimePickerFormat.Custom;
            pickTimeEnd.ShowUpDown =  true;
            pickTimeEnd.CustomFormat =  "HH:mm";
            pickTimeEnd.Value = DateTimePicker.MinimumDateTime +  tmBegin.m_t;
            foreach (eTariff e in tar)
                comboBox1.Items.Add(TarifString.fromEtarif(e));                   
            comboBox1.SelectedIndex = 0;
        }
        public void blockTimeChoose() {
            pickTimeEnd.Enabled = false;
        }
        public void setMinTime(TimeSpan t) {
            pickTimeEnd.MinDate = DateTimePicker.MinimumDateTime + t;
        }
        public void setMaxTime(TimeSpan t) {
            pickTimeEnd.MaxDate= DateTimePicker.MinimumDateTime + t;
        }


        private void ok_Click(object sender, EventArgs e) {

        }

        public CTariffIval getResult() {
            CTariffIval v = new CTariffIval();
            v.m_tar = TarifString.fromStr(comboBox1.SelectedItem.ToString());
            v.m_t = new TimeSpan(pickTimeEnd.Value.Hour, pickTimeEnd.Value.Minute, pickTimeEnd.Value.Second);
            return v;
        }

        private void label3_Click(object sender, EventArgs e) {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) {

        }

        private void label2_Click(object sender, EventArgs e) {

        }

        private void pickTimeEnd_ValueChanged(object sender, EventArgs e) {

        }

        private void cancel_Click(object sender, EventArgs e) {

        }
    }
}
