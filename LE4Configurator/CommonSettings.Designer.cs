﻿namespace LE4Configurator {
    partial class CommonSettings {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnWrPass = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.boxNewPass = new System.Windows.Forms.TextBox();
            this.boxOldPass = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnWrNetAddr = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.boxNetAddr = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnClrErr = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClrProfile = new System.Windows.Forms.Button();
            this.btnWrProfIval = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.upDwnRecIval = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.upDwnRecIval)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnWrPass);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.boxNewPass);
            this.groupBox1.Controls.Add(this.boxOldPass);
            this.groupBox1.Location = new System.Drawing.Point(23, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(336, 140);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Пароль администратора - восемь десятичных цифр";
            // 
            // btnWrPass
            // 
            this.btnWrPass.Location = new System.Drawing.Point(164, 44);
            this.btnWrPass.Name = "btnWrPass";
            this.btnWrPass.Size = new System.Drawing.Size(75, 23);
            this.btnWrPass.TabIndex = 5;
            this.btnWrPass.Text = "Записать";
            this.btnWrPass.UseVisualStyleBackColor = true;
            this.btnWrPass.Click += new System.EventHandler(this.btnWrPass_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Новый пароль";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Подтвердить пароль";
            // 
            // boxNewPass
            // 
            this.boxNewPass.Location = new System.Drawing.Point(9, 98);
            this.boxNewPass.Name = "boxNewPass";
            this.boxNewPass.PasswordChar = '*';
            this.boxNewPass.Size = new System.Drawing.Size(100, 20);
            this.boxNewPass.TabIndex = 2;
            // 
            // boxOldPass
            // 
            this.boxOldPass.Location = new System.Drawing.Point(9, 44);
            this.boxOldPass.Name = "boxOldPass";
            this.boxOldPass.PasswordChar = '*';
            this.boxOldPass.Size = new System.Drawing.Size(100, 20);
            this.boxOldPass.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnWrNetAddr);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.boxNetAddr);
            this.groupBox2.Location = new System.Drawing.Point(23, 221);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(336, 90);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Сетевой адрес";
            // 
            // btnWrNetAddr
            // 
            this.btnWrNetAddr.Location = new System.Drawing.Point(164, 44);
            this.btnWrNetAddr.Name = "btnWrNetAddr";
            this.btnWrNetAddr.Size = new System.Drawing.Size(75, 23);
            this.btnWrNetAddr.TabIndex = 5;
            this.btnWrNetAddr.Text = "Записать";
            this.btnWrNetAddr.UseVisualStyleBackColor = true;
            this.btnWrNetAddr.Click += new System.EventHandler(this.btnWrNetAddr_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(194, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Номер - до восьми десятичных цифр";
            // 
            // boxNetAddr
            // 
            this.boxNetAddr.Location = new System.Drawing.Point(12, 47);
            this.boxNetAddr.Name = "boxNetAddr";
            this.boxNetAddr.Size = new System.Drawing.Size(100, 20);
            this.boxNetAddr.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.btnClrErr);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.btnClrProfile);
            this.groupBox3.Location = new System.Drawing.Point(388, 31);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(278, 140);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Очистка ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Ошибки";
            // 
            // btnClrErr
            // 
            this.btnClrErr.Location = new System.Drawing.Point(184, 72);
            this.btnClrErr.Name = "btnClrErr";
            this.btnClrErr.Size = new System.Drawing.Size(75, 23);
            this.btnClrErr.TabIndex = 5;
            this.btnClrErr.Text = "Очистка журналов";
            this.btnClrErr.UseVisualStyleBackColor = true;
            this.btnClrErr.Click += new System.EventHandler(this.btnClrErr_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Профиль мощности";
            // 
            // btnClrProfile
            // 
            this.btnClrProfile.Location = new System.Drawing.Point(184, 19);
            this.btnClrProfile.Name = "btnClrProfile";
            this.btnClrProfile.Size = new System.Drawing.Size(75, 23);
            this.btnClrProfile.TabIndex = 0;
            this.btnClrProfile.Text = "Очистить";
            this.btnClrProfile.UseVisualStyleBackColor = true;
            this.btnClrProfile.Click += new System.EventHandler(this.btnClrProfile_Click);
            // 
            // btnWrProfIval
            // 
            this.btnWrProfIval.Location = new System.Drawing.Point(184, 34);
            this.btnWrProfIval.Name = "btnWrProfIval";
            this.btnWrProfIval.Size = new System.Drawing.Size(75, 23);
            this.btnWrProfIval.TabIndex = 0;
            this.btnWrProfIval.Text = "Записать";
            this.btnWrProfIval.UseVisualStyleBackColor = true;
            this.btnWrProfIval.Click += new System.EventHandler(this.btnWrProfIval_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.upDwnRecIval);
            this.groupBox4.Controls.Add(this.btnWrProfIval);
            this.groupBox4.Location = new System.Drawing.Point(388, 225);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(278, 86);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Интервал записи профиля, мин";
            // 
            // upDwnRecIval
            // 
            this.upDwnRecIval.Location = new System.Drawing.Point(22, 34);
            this.upDwnRecIval.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.upDwnRecIval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.upDwnRecIval.Name = "upDwnRecIval";
            this.upDwnRecIval.Size = new System.Drawing.Size(120, 20);
            this.upDwnRecIval.TabIndex = 1;
            this.upDwnRecIval.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // CommonSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CommonSettings";
            this.Size = new System.Drawing.Size(1040, 600);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.upDwnRecIval)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox boxNewPass;
        private System.Windows.Forms.TextBox boxOldPass;
        private System.Windows.Forms.Button btnWrPass;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnWrNetAddr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox boxNetAddr;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClrProfile;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnClrErr;
        private System.Windows.Forms.Button btnWrProfIval;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown upDwnRecIval;
    }
}
