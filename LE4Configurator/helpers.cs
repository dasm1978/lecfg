﻿using System;
using System.Windows.Forms;

namespace LE4Configurator {

    public static class Utilities {
        public static void MakeEq(DataGridView dataGridView) {
            dataGridView.RowHeadersWidth = 120;
            if (dataGridView.ColumnCount == 0) return;
            int w = (dataGridView.Width - dataGridView.RowHeadersWidth) / dataGridView.ColumnCount; // тут прикол с DPI                
            w =( dataGridView.Parent.Width - dataGridView.RowHeadersWidth) / dataGridView.ColumnCount; // тут прикол с DPI                

            

            foreach (DataGridViewColumn v in dataGridView.Columns) {
                v.Width = w;
            }
            //StretchLastColumn(dataGridView);
        }
        public static void StretchLastColumn(DataGridView dataGridView) {
            var lastColIndex = dataGridView.Columns.Count - 1;
            var lastCol = dataGridView.Columns[lastColIndex];
            foreach (DataGridViewColumn c in dataGridView.Columns)
                c.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            lastCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
    }

    static class BCD {
        static public byte[] bin2bcd(byte[] n) {
            byte[] bb = new byte[n.Length];
            for (int i = 0; i < bb.Length; i++) {
                bb[i] = bin2bcd(n[i]);
            }
            return bb;
        }
        static public byte bin2bcd(byte n) {
            return (byte)((((n) / 10) << 4) + ((n) % 10));
        }
        static public byte bin2bcd(int n) {
            return bin2bcd((byte)n);
        }

        static public byte bcd2bin(byte n) {
            return (byte)(((n) >> 4) * 10 + ((n) & 0x0F));
        }

        static public byte[] bcd2bin(byte[] n) {
            byte[] bb = new byte[n.Length];
            for (int i = 0; i < bb.Length; i++) {
                bb[i] = bcd2bin(n[i]);
            }
            return bb;
        }

        static public byte bcd2bin(int n) {
            return bcd2bin((byte)n);
        }

    }


    class CDateTime {
        public CDateTime() {
            m_dt = DateTime.Now;
        }

        public CDateTime(DateTime dt) {
            m_dt = dt;
        }
        public DateTime dt {
            set {
                m_dt = value;
            }
            get {
                return m_dt;
            }
        }
        public int rawLen { get { return 7; } }
        public byte[] asBCDRaw {
            set {
                int s = BCD.bcd2bin(value[0]);
                int m = BCD.bcd2bin(value[1]);
                int h = BCD.bcd2bin(value[2]);
                int dm = BCD.bcd2bin(value[3]);
                int mn = BCD.bcd2bin(value[4]);
                int y = BCD.bcd2bin(value[5]);
                DateTime newDT = new DateTime(y + 1900, mn, dm, h, m, s);
                m_dt = newDT;
            }
            get {
                byte[] raw = new byte[rawLen];
                int dst = 0;
                raw[dst] = BCD.bin2bcd(m_dt.Second); dst++;
                raw[dst] = BCD.bin2bcd(m_dt.Minute); dst++;
                raw[dst] = BCD.bin2bcd(m_dt.Hour); dst++;
                raw[dst] = BCD.bin2bcd(m_dt.Day); dst++;
                raw[dst] = BCD.bin2bcd(m_dt.Month - 1); dst++;
                raw[dst] = BCD.bin2bcd(m_dt.Year - 1900); dst++;
                raw[dst] = BCD.bin2bcd((byte)m_dt.DayOfWeek);
                // Array.Copy(BitConverter.GetBytes(m_dt.Hour), 0, raw, dst, 4); dst += 1;
                return raw;
            }
        }
        private DateTime m_dt;
    }

    class sRealMeasurements {
        public float curU;
        public float curI;
        public float curP;
        public float curQ;
        public float freq;
        public float curEnergy;
        public float cosFi;
        public float curQEnergy;
        public static int rawLen { get { return 8 * 4; } }
        public byte[] asRaw {
            set {
                int src = 0;
                curU = BitConverter.ToSingle(value, src); src += 4;
                curI = BitConverter.ToSingle(value, src); src += 4;
                curP = BitConverter.ToSingle(value, src); src += 4;
                curQ = BitConverter.ToSingle(value, src); src += 4;
                freq = BitConverter.ToSingle(value, src); src += 4;
                curEnergy = BitConverter.ToSingle(value, src); src += 4;
                curQEnergy = BitConverter.ToSingle(value, src); src += 4;
                cosFi = BitConverter.ToSingle(value, src); src += 4;
            }
        }

    }


}
