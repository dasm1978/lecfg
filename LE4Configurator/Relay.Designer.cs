﻿namespace LE4Configurator {
    partial class Relay {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnOffRelay = new System.Windows.Forms.Button();
            this.btnOnRelay = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.boxPower = new LE4Configurator.NumTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.boxOffTime = new LE4Configurator.NumTextBox();
            this.boxAboveTime = new LE4Configurator.NumTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.checkPowLim = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkLimEn4 = new System.Windows.Forms.CheckBox();
            this.boxLimEn4 = new LE4Configurator.NumTextBox();
            this.boxLimEn3 = new LE4Configurator.NumTextBox();
            this.boxLimEn2 = new LE4Configurator.NumTextBox();
            this.boxLimEn1 = new LE4Configurator.NumTextBox();
            this.chkLimEn3 = new System.Windows.Forms.CheckBox();
            this.chkLimEn2 = new System.Windows.Forms.CheckBox();
            this.chkLimEn1 = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnOffRelay);
            this.groupBox1.Controls.Add(this.btnOnRelay);
            this.groupBox1.Location = new System.Drawing.Point(16, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(264, 94);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ручное";
            // 
            // btnOffRelay
            // 
            this.btnOffRelay.Location = new System.Drawing.Point(116, 44);
            this.btnOffRelay.Name = "btnOffRelay";
            this.btnOffRelay.Size = new System.Drawing.Size(75, 23);
            this.btnOffRelay.TabIndex = 0;
            this.btnOffRelay.Text = "Выкл";
            this.btnOffRelay.UseVisualStyleBackColor = true;
            // 
            // btnOnRelay
            // 
            this.btnOnRelay.Location = new System.Drawing.Point(24, 44);
            this.btnOnRelay.Name = "btnOnRelay";
            this.btnOnRelay.Size = new System.Drawing.Size(75, 23);
            this.btnOnRelay.TabIndex = 0;
            this.btnOnRelay.Text = "Вкл";
            this.btnOnRelay.UseVisualStyleBackColor = true;
            this.btnOnRelay.Click += new System.EventHandler(this.btnOnRelay_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.boxPower);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.boxOffTime);
            this.groupBox2.Controls.Add(this.boxAboveTime);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.checkPowLim);
            this.groupBox2.Location = new System.Drawing.Point(16, 150);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(264, 207);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Лимит мощности";
            // 
            // boxPower
            // 
            this.boxPower.Location = new System.Drawing.Point(6, 70);
            this.boxPower.Name = "boxPower";
            this.boxPower.ShortcutsEnabled = false;
            this.boxPower.Size = new System.Drawing.Size(75, 20);
            this.boxPower.TabIndex = 4;
            this.boxPower.Text = "0";
            
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(87, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Мощность, Вт";
            // 
            // boxOffTime
            // 
            this.boxOffTime.Location = new System.Drawing.Point(6, 158);
            this.boxOffTime.Name = "boxOffTime";
            this.boxOffTime.ShortcutsEnabled = false;
            this.boxOffTime.Size = new System.Drawing.Size(75, 20);
            this.boxOffTime.TabIndex = 1;
            this.boxOffTime.Text = "0";
            // 
            // boxAboveTime
            // 
            this.boxAboveTime.Location = new System.Drawing.Point(6, 112);
            this.boxAboveTime.Name = "boxAboveTime";
            this.boxAboveTime.ShortcutsEnabled = false;
            this.boxAboveTime.Size = new System.Drawing.Size(75, 20);
            this.boxAboveTime.TabIndex = 1;
            this.boxAboveTime.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(87, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Длительность отключения, мин";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(87, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Длительность превышения, с";
            // 
            // checkPowLim
            // 
            this.checkPowLim.AutoSize = true;
            this.checkPowLim.Location = new System.Drawing.Point(6, 36);
            this.checkPowLim.Name = "checkPowLim";
            this.checkPowLim.Size = new System.Drawing.Size(125, 17);
            this.checkPowLim.TabIndex = 0;
            this.checkPowLim.Text = "Включить контроль";
            this.checkPowLim.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Тариф 1, Вт*ч";
            
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Тариф 2, Вт*ч";
            
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Тариф 3, Вт*ч";
            
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Тариф 4, Вт*ч";
            
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkLimEn4);
            this.groupBox3.Controls.Add(this.boxLimEn4);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.boxLimEn3);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.boxLimEn2);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.boxLimEn1);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.chkLimEn3);
            this.groupBox3.Controls.Add(this.chkLimEn2);
            this.groupBox3.Controls.Add(this.chkLimEn1);
            this.groupBox3.Location = new System.Drawing.Point(389, 23);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(355, 334);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Лимит энергии";
            // 
            // chkLimEn4
            // 
            this.chkLimEn4.AutoSize = true;
            this.chkLimEn4.Location = new System.Drawing.Point(208, 149);
            this.chkLimEn4.Name = "chkLimEn4";
            this.chkLimEn4.Size = new System.Drawing.Size(125, 17);
            this.chkLimEn4.TabIndex = 4;
            this.chkLimEn4.Text = "Включить контроль";
            this.chkLimEn4.UseVisualStyleBackColor = true;
            
            // 
            // boxLimEn4
            // 
            this.boxLimEn4.Location = new System.Drawing.Point(108, 146);
            this.boxLimEn4.Name = "boxLimEn4";
            this.boxLimEn4.ShortcutsEnabled = false;
            this.boxLimEn4.Size = new System.Drawing.Size(75, 20);
            this.boxLimEn4.TabIndex = 3;
            this.boxLimEn4.Text = "0";
            
            // 
            // boxLimEn3
            // 
            this.boxLimEn3.Location = new System.Drawing.Point(108, 104);
            this.boxLimEn3.Name = "boxLimEn3";
            this.boxLimEn3.ShortcutsEnabled = false;
            this.boxLimEn3.Size = new System.Drawing.Size(75, 20);
            this.boxLimEn3.TabIndex = 3;
            this.boxLimEn3.Text = "0";
            
            // 
            // boxLimEn2
            // 
            this.boxLimEn2.Location = new System.Drawing.Point(108, 62);
            this.boxLimEn2.Name = "boxLimEn2";
            this.boxLimEn2.ShortcutsEnabled = false;
            this.boxLimEn2.Size = new System.Drawing.Size(75, 20);
            this.boxLimEn2.TabIndex = 3;
            this.boxLimEn2.Text = "0";
            
            // 
            // boxLimEn1
            // 
            this.boxLimEn1.Location = new System.Drawing.Point(108, 20);
            this.boxLimEn1.Name = "boxLimEn1";
            this.boxLimEn1.ShortcutsEnabled = false;
            this.boxLimEn1.Size = new System.Drawing.Size(75, 20);
            this.boxLimEn1.TabIndex = 3;
            this.boxLimEn1.Text = "0";
            
            // 
            // chkLimEn3
            // 
            this.chkLimEn3.AutoSize = true;
            this.chkLimEn3.Location = new System.Drawing.Point(208, 107);
            this.chkLimEn3.Name = "chkLimEn3";
            this.chkLimEn3.Size = new System.Drawing.Size(125, 17);
            this.chkLimEn3.TabIndex = 3;
            this.chkLimEn3.Text = "Включить контроль";
            this.chkLimEn3.UseVisualStyleBackColor = true;
            
            // 
            // chkLimEn2
            // 
            this.chkLimEn2.AutoSize = true;
            this.chkLimEn2.Location = new System.Drawing.Point(208, 65);
            this.chkLimEn2.Name = "chkLimEn2";
            this.chkLimEn2.Size = new System.Drawing.Size(125, 17);
            this.chkLimEn2.TabIndex = 3;
            this.chkLimEn2.Text = "Включить контроль";
            this.chkLimEn2.UseVisualStyleBackColor = true;
            
            // 
            // chkLimEn1
            // 
            this.chkLimEn1.AutoSize = true;
            this.chkLimEn1.Location = new System.Drawing.Point(208, 23);
            this.chkLimEn1.Name = "chkLimEn1";
            this.chkLimEn1.Size = new System.Drawing.Size(125, 17);
            this.chkLimEn1.TabIndex = 3;
            this.chkLimEn1.Text = "Включить контроль";
            this.chkLimEn1.UseVisualStyleBackColor = true;
            
            // 
            // Relay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Relay";
            this.Size = new System.Drawing.Size(854, 566);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnOffRelay;
        private System.Windows.Forms.Button btnOnRelay;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkPowLim;
        private System.Windows.Forms.Label label1;
        private NumTextBox boxOffTime;
        private NumTextBox boxAboveTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chkLimEn4;
        private NumTextBox boxLimEn4;
        private NumTextBox boxLimEn3;
        private NumTextBox boxLimEn2;
        private NumTextBox boxLimEn1;
        private System.Windows.Forms.CheckBox chkLimEn3;
        private System.Windows.Forms.CheckBox chkLimEn2;
        private System.Windows.Forms.CheckBox chkLimEn1;
        private NumTextBox boxPower;
    }
}
